<?php get_header(); ?>
<main class="pt_bg_dot">
	
<section class="pd-common parallax" data-parallax-bg-image="<?php echo get_template_directory_uri(); ?>/img/about_fv.jpg" data-parallax-bg-position="center" data-parallax-speed="0.4" data-parallax-direction="down">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<div class="under_fv_txtarea pt_bg_white mt140 mt-xs-80 mb50 pt_br" data-aos="fade-up">
					<p class="engTitle h1 mainColor relative">About Us</p>
					<h2 class="jpTitle h1 bold">高塚について</h2>
				</div>
			</div>
		</div>
	</div>
</section>
	
<section class="pd-common">
	<div class="container">
		<div class="row">
			<div class="col-sm-12" data-aos="fade-up">
				<div class="text-center">
					<p class="pt_eng_title engTitle h1 mainColor relative">Concept</p>
					<h3 class="jpTitle h1 mainColor bold mb50 mb-xs-20">当たり前のことを、<br>お客さま目線で一から考える</h3>
				</div>
				<p class="width720 text-center text-left-xs mb10 h4 bold">「髙塚に頼めば、うまくやってくれるよ」。</p>
				<p class="width720 text-center text-left-xs mb20">工事をご依頼くださったお客さまのこの言葉は、<br>たくさんの新しいお客さまとのつながりを生み、<br>私たちは長く地域に根ざした営業を続けてくることができました。</p>
				<p class="width720 text-center text-left-xs mb20">お客さまからの高い評価と絶大な信頼を築いたのは、<br>創業者である父の仕事に対する姿勢。<br>職人として40年以上のキャリアを持ちながら、<br>現場ではいつも、「どうすればこの工事はもっとよく仕上がるだろうか？」と、<br>ブロックの積み方ひとつからお客さま目線に立って考えていました。</p>
				<p class="width720 text-center text-left-xs mb50">プロであれば、いつも通りにやればできる工事を、<br>必ず最初の一歩から最高の結果を目指して考える。<br>髙塚が現在、お客さまから引きも切らずにお声がけいただいているのは、<br>ひとえに父のこの姿勢があったからだと感じています。</p>
				<p class="width720 text-center text-left-xs mb50">新たな時代を迎える今、私たちが目指すのは、<br>これまで築いたお客さまからの信頼を守り、これから出会うお客さまにもまた、<br>「よかった」と笑顔で完成を見届けていただくこと。<br>信頼を守ることの難しさを心に刻みながら、<br>新しい出会いのひとつひとつをこれまで通り大切にしてまいります。</p>
			</div>
		</div>
	</div>
	<ul class="top_staff_ul ul-3 ul-xs-1 mb50">
		<li>
			<div class="top_staff_img_outer" data-aos="fade-right">
				<div class="top_staff_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/about_concept01.jpg');"></div>
			</div>
		</li>
		<li>
			<div class="top_staff_img_outer" data-aos="fade-up">
				<div class="top_staff_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/about_concept02.jpg');"></div>
			</div>
		</li>
		<li>
			<div class="top_staff_img_outer" data-aos="fade-left">
				<div class="top_staff_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/about_concept03.jpg');"></div>
			</div>
		</li>
	</ul>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="about_message_area width980 bgLightBlueColor pt_br" data-aos="fade-up">
					<ul class="about_message_ul ul-2 ul-xs-1">
						<li>
							<div class="about_message_img bg-common pt_br" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/about_concept_top.jpg');"></div>
						</li>
						<li>
							<p class="white mb10">代表取締役</p>
							<p class="h3 white mb20">髙塚 雅好　<span class="h5 engTitle">Takatsuka Masayoshi</span></p>
							<p class="white">家業を手伝いはじめたのは小学生の頃。創業者である父の「助かった」と、お客さまからの「ありがとう」に支えられ、1998年に20歳で入社しました。2019年6月に代表取締役に就任。髙塚がこれまで築いたお客さまの信頼を守り、確かな施工でこれからも地域の安心を支えてまいります。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
	
<section class="pd-common pb0">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">	
				<div class="text-center">
					<p class="pt_eng_title engTitle h1 mainColor relative">Feature</p>
					<h3 class="jpTitle h1 mainColor bold mb50 mb-xs-20">仕事へのこだわり</h3>
				</div>
			</div>
		</div>
	</div>
	<ul class="service_feature_ul ul-2 ul-xs-1 flex mb0">
		<li class="bgLightBlueColor relative" data-aos="fade-right">
			<div class="text-center-xs mb30"><h4 class="jpTitle h1 white bold pt_marker mb30 mb-xs-10 inline">完全オーダーメイドの柔軟なご提案</h4></div>
			<p class="numTitle service_feature_num semibold italic absolute">01</p>
			<p class="service_feature_txt white lh_l relative">髙塚では、ご予算とご希望に合わせた完全オーダーメイドのプランをご提案しています。ライトの設置やカーポートの修繕などの小規模な工事から、数百メートル規模のフェンスの設置など。予算や規模の大小に関わらず、高い技術で誠実な施工を行っています。「家まわり」のお悩みは、どんなことでもお気軽にご相談ください。</p>
		</li>
		<li class="bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/about_feature01.jpg');" data-aos="fade-left"></li>
	</ul>
	<ul class="service_feature_ul service_feature_ul_even ul-2 ul-xs-1 flex mb0" >
		<li class="bgLightBlueColor relative" data-aos="fade-left">
			<div class="text-center-xs mb30"><h4 class="jpTitle h1 white bold pt_marker mb30 mb-xs-10 inline">クレームなし、アフター不要の施工</h4></div>
			<p class="numTitle service_feature_num semibold italic absolute">02</p>
			<p class="service_feature_txt white lh_l relative">弊社では何度も同じお客さま宅へ修理にお伺いするのではなく、これまで施工したお客さまからのご紹介で、新しいお客さまからのご依頼が生まれています。工事に対するクレームと、アフターメンテナンスの必要がない確実な施工が、お客さまからの高い評価につながっています。</p>
		</li>
		<li class="bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/about_feature02.jpg');" data-aos="fade-right"></li>
	</ul>
	<ul class="service_feature_ul ul-2 ul-xs-1 flex mb0">
		<li class="bgLightBlueColor relative" data-aos="fade-right">
			<div class="text-center-xs mb30"><h4 class="jpTitle h1 white bold pt_marker mb30 mb-xs-10 inline">地域に密着したスピーディーな対応</h4></div>
			<p class="numTitle service_feature_num semibold italic absolute">03</p>
			<p class="service_feature_txt white lh_l relative">髙塚は総社を中心に地域と密着し、業者やメーカーとの連携を深めてきました。お客さまからの「ちょっと見てほしい」には、スピーディーに対応。小さなお悩みやささやかな使い勝手の悪さをこまめに解決することで、「いつもの安全な暮らし」を末永く守ります。</p>
		</li>
		<li class="bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/about_feature03.jpg');" data-aos="fade-left"></li>
	</ul>
</section>
	
<section class="pd-common bgWhiteColor" id="company_description">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center">
					<p class="pt_eng_title engTitle h1 mainColor relative">Company</p>
					<h3 class="jpTitle h1 mainColor bold mb50 mb-xs-20">会社概要</h3>
				</div>
				<div class="pageAboutCompanyUl width720 mb50">
					<ul>
						<li>会社名</li>
						<li>有限会社 髙塚</li>
					</ul>
					<ul>
						<li>所在地</li>
						<li>〒719-1124<br>岡山県総社市三須616-23</li>
					</ul>
					<ul>
						<li>連絡先</li>
						<li>TEL：0866-93-7821<br>FAX：0866-93-1109</li>
					</ul>
					<ul>
						<li>営業時間</li>
						<li>9:00～17:00</li>
					</ul>
					<ul>
						<li>代表取締役</li>
						<li>髙塚 雅好</li>
					</ul>
					<ul>
						<li>事業内容</li>
						<li>エクステリア工事全般</li>
					</ul>
				</div>
			</div>
		</div>
	</div>	
</section>

<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	

</main>

<?php get_footer(); ?>