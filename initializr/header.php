<!doctype html>
<html>
<head>
<meta charset="utf-8">
<!--キャッシュクリア-->
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="0">
<!--キャッシュクリア終了-->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title('',true); ?><?php if(wp_title('',false)) { ?> | <?php } ?><?php bloginfo('name'); ?></title>
<?php wp_head(); ?>
<link rel="icon" type="image/x-icon" href="<?php echo get_template_directory_uri();?>/favicon.ico">
<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri();?>/apple-touch-icon.png">
<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/modal.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/loaders.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/bootstrap.min.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=EB+Garamond" rel="stylesheet">
<!--<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">-->
<link href="https://fonts.googleapis.com/css?family=Bebas+Neue&display=swap" rel="stylesheet">
<!--<link href="https://fonts.googleapis.com/earlyaccess/hannari.css" rel="stylesheet">-->
<link href="https://fonts.googleapis.com/css?family=Sacramento&display=swap" rel="stylesheet">
<script type="text/javascript" src="//webfont.fontplus.jp/accessor/script/fontplus.js?tUOPpkwcnxA%3D&box=DPC0QA6Ps9Q%3D&aa=1&ab=2" charset="utf-8"></script>

<link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans:600i&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Comfortaa&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/main.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/style.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/slick-theme.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/lightbox.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/style.css">

<script src="<?php echo get_template_directory_uri();?>/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo get_template_directory_uri();?>/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
<script src="<?php echo get_template_directory_uri();?>/js/vendor/bootstrap.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/jquery.matchHeight-min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.1/TweenMax.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/parallax/parallax.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/main.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/slick.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/lightbox.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.1/TweenMax.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/wavify/wavify.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri();?>/js/wavify/jquery.wavify.js" type="text/javascript"></script>
	<script src="<?php echo get_template_directory_uri();?>/js/parallax-background.min.js"></script>
	<script src="<?php echo get_template_directory_uri();?>/js/jquery.particleground.min.js"></script>
<script>
/*高さ合わせる*/
$(function(){
	$('.matchHeight').matchHeight();
});

</script>
<script type="text/javascript">
$(function() {
	$('.slick-box').slick({
		prevArrow: '<div class="slider-arrow slider-prev fa fa-angle-left"></div>',
		nextArrow: '<div class="slider-arrow slider-next fa fa-angle-right"></div>',
		autoplay: true,
		autoplaySpeed: 4000,
		fade: true,
		speed: 2000,
	});
});
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-55DJ9HT');</script>
<!-- End Google Tag Manager -->

</head>

<body <?php body_class();?>>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-55DJ9HT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<!--ローディングCSS-->
<!--
<div id="js-loader">
	<div class="loader-inner ball-pulse">
	  <div></div>
	  <div></div>
	  <div></div>
	</div>
</div>
-->
<style>
#js-loader {
	display: flex;
    position: fixed;
    height: 100vh;
    width: 100vw;
    z-index: 9999;
    background: #1ba0e5;
    align-items: center;
    justify-content: center;
}

.ball-pulse-sync>div, .ball-pulse>div, .ball-scale-random>div, .ball-scale>div {
    background-color: #fff;
}
</style>

<script>
// ローディング画面をフェードインさせてページ遷移
$(function(){
    // リンクをクリックしたときの処理。外部リンクやページ内移動のスクロールリンクなどではフェードアウトさせたくないので少し条件を加えてる。
    $('a[href ^= "https://newstella.co.jp"]' + 'a[target != "_blank"]').click(function(){
        var url = $(this).attr('href'); // クリックされたリンクのURLを取得
        $('#js-loader').fadeIn(600);    // ローディング画面をフェードイン
        setTimeout(function(){ location.href = url; }, 800); // URLにリンクする
        return false;
    });
});

// ページのロードが終わった後の処理
$(window).load(function(){
  $('#js-loader').delay(300).fadeOut(400); //ローディング画面をフェードアウトさせることでメインコンテンツを表示
});
 
// ページのロードが終わらなくても10秒たったら強制的に処理を実行
$(function(){ setTimeout('stopload()', 10000); });
function stopload(){
  $('#js-loader').delay(300).fadeOut(400); //ローディング画面をフェードアウトさせることでメインコンテンツを表示
}

</script>





<header>
	<div class="container">
		<div class="headerTop flex alignStart justBetween">
			<div class="headerLeft">
				<div class="headerText"><h2 class="white text_s">岡山・倉敷・総社の<span class="pc">外構・</span>エクステリア<span class="pc">・お庭</span>の工事なら</h2></div>
				<h1 class="logo remove"><a href="<?php echo home_url();?>"><?php wp_title('',true); ?><?php if(wp_title('',false)) { ?> | <?php } ?><?php bloginfo('name'); ?></a></h1>
			</div>
			<div class="headerMenu pc text-center bold relative">
				<?php wp_nav_menu( array( 'menu_class' => 'flex justEnd alignCenter titleJp engTitle' ) ); ?>
			</div>
		</div>
	</div>
</header>

<!-- 開閉用ボタン -->

<div class="menu-btn sp" id="js__btn">
    <!--<span data-txt-menu="MENU" data-txt-close="CLOSE"></span>-->
	<div class="menu-trigger" href="#">
		<span></span>
		<span></span>
		<span></span>
		<span class="headerHunbergerText">MENU</span>
	</div>	
</div>

<!-- モーダルメニュー -->
<nav class="menu sp" id="js__nav">
	<?php wp_nav_menu( array( 'menu_class' => '' ) ); ?>
</nav>


<section class="contactSide absolute pc">
	<a href="<?php echo home_url();?>/contact" class="contactSideWap text-center">
		<div class="fontEn mainColor h5 titleBd">mail</div>
		<div class="titleJp mColor text_s grayColor">メール<span class="">は<br>こちらから</span></div>
	</a>
	
</section>



<section class="footerTelSp sp bgMainColor">
	<ul class="LiInlineBlock">
		<li class="footerTelSpLi1 white text-center text-center-xs">
			<div class="text_ss">ご予約・お問い合わせはこちら</div>
			<div class="footerTelSpLi1Tel h3 bold">
				<a href="tel:0866937821"><i class="fa fa-phone" aria-hidden="true"></i><span>0866-93-7821</span></a>
			</div>
		</li >
		<li class="footerTelSpLi2 text_m mainColor"><a href="<?php echo home_url();?>/contact">お問い合わせ</a></li>
	</ul>
	
</section>

<script>
$(function () {
    var $body = $('body');

    //開閉用ボタンをクリックでクラスの切替え
    $('#js__btn').on('click', function () {
        $body.toggleClass('open');
    });

    //メニュー名以外の部分をクリックで閉じる
    $('#js__nav').on('click', function () {
        $body.removeClass('open');
    });
});

</script>
	
<script>
	$('.jsPW').wavify({
		height: 60,
		bones: 3,
		amplitude: 40,
		color: '#fff',
		speed: .25
	});
</script>

<script>
$(function() {
	$(window).on('load resize', function() {
		$('.parallax').parallaxBackground();
	});
});
</script>