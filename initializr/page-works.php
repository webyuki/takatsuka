<?php get_header(); ?>

<main class="pt_bg_dot">
	
<section class="pd-common parallax" data-parallax-bg-image="<?php echo get_template_directory_uri(); ?>/img/works_fv.jpg" data-parallax-bg-position="center" data-parallax-speed="0.4" data-parallax-direction="down">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<div class="under_fv_txtarea pt_bg_white mt140 mt-xs-80 mb50 pt_br">
					<p class="engTitle h1 mainColor relative">Works</p>
					<h2 class="jpTitle h1 bold">施工事例</h2>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="pd-common">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<ul class="list_link mb50">
					<li>
						<a href="" class="yellowBorderColor">カーポート設置</a>
					</li>
					<li>
						<a href="" class="yellowBorderColor">外構工事</a>
					</li>
					<li>
						<a href="" class="yellowBorderColor">外構リフォーム</a>
					</li>
				</ul>
				<ul class="top_works_ul ul-3 ul-sm-2 ul-xs-1 mb50">
					<li>
						<a href="<?php echo home_url(); ?>/detail">
							<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
								<div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
								<p class="h4 jpTitle mainColor bold mb10">岡山市 M様邸 カーポート設置</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">岡山市</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">カーポート設置</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">岡山市</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">カーポート設置</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/detail">
							<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
								<div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
								<p class="h4 jpTitle mainColor bold mb10">岡山市 M様邸 カーポート設置</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">岡山市</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">カーポート設置</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/detail">
							<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
								<div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
								<p class="h4 jpTitle mainColor bold mb10">岡山市 M様邸 カーポート設置</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">岡山市</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">カーポート設置</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/detail">
							<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
								<div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
								<p class="h4 jpTitle mainColor bold mb10">岡山市 M様邸 カーポート設置</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">岡山市</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">カーポート設置</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/detail">
							<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
								<div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
								<p class="h4 jpTitle mainColor bold mb10">岡山市 M様邸 カーポート設置</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">岡山市</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">カーポート設置</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/detail">
							<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
								<div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
								<p class="h4 jpTitle mainColor bold mb10">岡山市 M様邸 カーポート設置</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">岡山市</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">カーポート設置</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/detail">
							<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
								<div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
								<p class="h4 jpTitle mainColor bold mb10">岡山市 M様邸 カーポート設置</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">岡山市</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">カーポート設置</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/detail">
							<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
								<div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
								<p class="h4 jpTitle mainColor bold mb10">岡山市 M様邸 カーポート設置</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">岡山市</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">カーポート設置</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/detail">
							<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
								<div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
								<p class="h4 jpTitle mainColor bold mb10">岡山市 M様邸 カーポート設置</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">岡山市</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">カーポート設置</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/detail">
							<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
								<div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
								<p class="h4 jpTitle mainColor bold mb10">岡山市 M様邸 カーポート設置</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">岡山市</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">カーポート設置</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/detail">
							<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
								<div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
								<p class="h4 jpTitle mainColor bold mb10">岡山市 M様邸 カーポート設置</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">岡山市</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">カーポート設置</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/detail">
							<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
								<div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
								<p class="h4 jpTitle mainColor bold mb10">岡山市 M様邸 カーポート設置</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">岡山市</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">カーポート設置</p>
							</div>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>

</main>






<?php get_footer(); ?>