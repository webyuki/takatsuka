                    <li>
						<a href="<?php the_permalink(); ?>">
							<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
							
                                <?php if (has_post_thumbnail()):?>
                                    <?php 
                                        // アイキャッチ画像のIDを取得
                                        $thumbnail_id = get_post_thumbnail_id();
                                        // mediumサイズの画像内容を取得（引数にmediumをセット）
                                        $eye_img = wp_get_attachment_image_src( $thumbnail_id , 'full' );
                                        $eye_img_s = wp_get_attachment_image_src( $thumbnail_id , 'thumb_size_s_false',false );
                                    ?>
                                        <div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo $eye_img_s[0];?>');"></div>
                                    <?php else: ?>
                                        <div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/sample01.png');"></div>
                                <?php endif; ?>
							
								<p class="h4 jpTitle mainColor bold mb10"><?php the_title(); ?></p>
                                <?php 
                                    if ($terms = get_the_terms($post->ID, 'works_area')) {
                                        foreach ( $terms as $term ) {
                                            echo '<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">' . esc_html($term->name) .'</p>';
                                        }
                                    }
                                ?>
                                <?php 
                                    if ($terms = get_the_terms($post->ID, 'works_cate')) {
                                        foreach ( $terms as $term ) {
                                            echo '<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">' . esc_html($term->name) .'</p>';
                                        }
                                    }
                                ?>
							</div>
						</a>
					</li>