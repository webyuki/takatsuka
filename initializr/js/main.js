/*バナー追従コンタクト*/
$(function(){
	$(window).scroll(function(){
		var sc = $(window).scrollTop();
		if( sc > 300 ){
			$('#followBanner').fadeIn();
		}else{
			$('#followBanner').fadeOut();
		}
	});
});

/*iframeを一括置換*/
$(function(){
	$('.entry_cont > p > iframe').wrap('<div class="iframe_wrapper"></div>');
});

/*Q&Aの開閉*/
$(function(){
	$(".topQaBoxDl dt").on("click", function() {
		$(this).next().slideToggle();
	});
});


//スムーズスクロール
$(function() {
  // スクロールのオフセット値
  var offsetY = -10;
  // スクロールにかかる時間
  var time = 500;

  // ページ内リンクのみを取得
  $('a[href*="#"]').click(function() {
    // 移動先となる要素を取得
    var target = $(this.hash);
    if (!target.length) return ;
    // 移動先となる値
    var targetY = target.offset().top+offsetY;
    // スクロールアニメーション
    $('html,body').animate({scrollTop: targetY}, time, 'swing');
    // ハッシュ書き換えとく
    window.history.pushState(null, null, this.hash);
    // デフォルトの処理はキャンセル
    return false;
  });
});





// グローバル変数
var syncerTimeout = null ;
// 一連の処理
$( function()
{
	// スクロールイベントの設定
	$( window ).scroll( function()
	{
		// 1秒ごとに処理
		if( syncerTimeout == null )
		{
			// セットタイムアウトを設定
			syncerTimeout = setTimeout( function(){
				// 対象のエレメント
				var element = $( '#js_scrollTop' ) ;
				// 現在、表示されているか？
				var visible = element.is( ':visible' ) ;
				// 最上部から現在位置までの距離を取得して、変数[now]に格納
				var now = $( window ).scrollTop() ;
				// 最下部から現在位置までの距離を計算して、変数[under]に格納
				var under = $( 'body' ).height() - ( now + $(window).height() ) ;
				// 最上部から現在位置までの距離(now)が1500以上かつ
				// 最下部から現在位置までの距離(under)が200px以上かつ…
				if( now > 1500 && 200 < under )
				{
					// 非表示状態だったら
					if( !visible )
					{
						// [#page-top]をゆっくりフェードインする
						element.fadeIn( 'slow' ) ;
					}
				}
				// 1500px以下かつ
				// 表示状態だったら
				else if( visible )
				{
					// [#page-top]をゆっくりフェードアウトする
					element.fadeOut( 'slow' ) ;
				}
				// フラグを削除
				syncerTimeout = null ;
			} , 1000 ) ;
		}
	} ) ;
	// クリックイベントを設定する
	$( '#js_scrollTop' ).click(
		function()
		{
			// スムーズにスクロールする
			$( 'html,body' ).animate( {scrollTop:0} , 'slow' ) ;
		}
	) ;
} ) ;


//人気記事表示でbackgroundにimg指定
/*
$(function(){
	$('.wpp-list li').each(function() {
		var wppImg = $(this).find('img');
		var wppImgPare = $(wppImg).parents('.topArt__img.bgup') ;
		var wppImgSrc =$(wppImg).attr('src');
		var wppImgBg = "url(" + wppImgSrc + ")";
		$(wppImgPare).css('background-image',wppImgBg);
		$(wppImg).css('display','none');
	});
});
*/

/*ナビゲーションドロップダウンふわっと出現*/

/*
$(window).on('load resize', function(){
        var w = $(window).width();
        var x = 768;
        if (w < x) {
//画面サイズが768px未満のときの処理
        } else {
//それ以外のときの処理
        
			$(function(){
				$('.navbar-nav li').hover(
					function(){
						$(this).find('ul').fadeIn();
					},
					function(){
						$(this).find('ul').fadeOut();
					}
				);
			});
		
		}
      });

*/

/*googleアナリティクス イベントコンバージョン*/
$(function(){
	$("a").click(function(){        
		var ahref = $(this).attr('href');
		if (ahref.indexOf("webshugi.com") != -1 || ahref.indexOf("http") == -1 ) {
			ga('send', 'event', '内部リンク', 'クリック', ahref);
		}else if(ahref.indexOf("c.af.moshimo.com") != -1 ) {
			ga('send', 'event', 'もしもリンク', 'クリック', ahref);
		}else if(ahref.indexOf("googleads") != -1 || ahref.indexOf("dsp.send.microad") != -1 ) {
			ga('send', 'event', 'アドセンスリンク', 'クリック', ahref);			
		}else { 
			ga('send', 'event', '外部リンク', 'クリック', ahref);
		}
	});
});

/*フッターのナビゲーションにiconを追加*/
$(function(){
    $('.footerNavUl li').prepend('<i class="fa fa-arrow-circle-right"></i>');
});
