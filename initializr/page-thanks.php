<?php get_header(); ?>

<main>
	
<section class="pd-common parallax" data-parallax-bg-image="<?php echo get_template_directory_uri(); ?>/img/about_fv.jpg" data-parallax-bg-position="center" data-parallax-speed="0.4" data-parallax-direction="down">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<div class="under_fv_txtarea pt_bg_white mt140 mt-xs-80 mb50 pt_br">
					<p class="engTitle h1 mainColor relative">Thanks</p>
					<h2 class="jpTitle h1 bold">送信完了</h2>
				</div>
			</div>
		</div>
	</div>
</section>



<section class="pd-common relative paperBgUnder">
	<div class="container">
		<div class="">
			<div class="contInCont" data-aos="fade-up">
				<div class="mb30 text-center width780">
					<p>お問い合わせの送信が完了しました。</p>
					<p>担当者から折り返させてご連絡させて頂きますので、今しばらくお待ち下さい。</p>
				</div>
				<a class="telLink fontEn h0 text-center bold block mb30" href="tel:0866937821">0866-93-7821</a>
				<div class="contactForm" data-aos="fade-up"><?php echo do_shortcode('[mwform_formkey key="67"]'); ?></div>
			</div>
		</div>
	</div>
</section>


<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>