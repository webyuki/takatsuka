<?php get_header(); ?>
<main style="background-color: #ebebde;">
	
<section class="under_fv bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/under_detail_fv.png');">
	<h2 class="under_fv_title">作物について</h2>	
</section>

<section class="pd-common">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 mb50">
				<div class="detail_inner">
				<div class="text-center text-center-xs">
					<h3 class="detail_title mb20">おまかせパック</h3>
				</div>
				<div class="detail_img_area">
					<div class="detail_fv bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/detail_fv.jpg');" data-aos="fade-up"></div>
					<ul class="ul-3 ul-xs-2 detail_slider_ul" data-aos="fade-up">
						<li>
							<a href="<?php echo get_template_directory_uri(); ?>/img/detail_slider01.jpg" data-lightbox="detail"><div class="detail_slider bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/detail_slider01.jpg');"></div></a>
						</li>
						<li>
							<a href="<?php echo get_template_directory_uri(); ?>/img/detail_slider01.jpg" data-lightbox="detail"><div class="detail_slider bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/detail_slider01.jpg');"></div></a>
						</li>
						<li>
							<a href="<?php echo get_template_directory_uri(); ?>/img/detail_slider01.jpg" data-lightbox="detail"><div class="detail_slider bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/detail_slider01.jpg');"></div></a>
						</li>
						<li>
							<a href="<?php echo get_template_directory_uri(); ?>/img/detail_slider01.jpg" data-lightbox="detail"><div class="detail_slider bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/detail_slider01.jpg');"></div></a>
						</li>
					</ul>
					<script>
						$('.detail_slider_ul').slick({
							autoplay:false,
							autoplaySpeed:5000,
							dots:false,
							slidesToShow:3,
							slidesToScroll:1,
							
							// 以下、レスポンシブ
							responsive: [
								{
									breakpoint: 767,
							  		settings: {
										slidesToShow: 3,
							  		}
								}
							]
						});
					</script>
				</div>
			<div class="row mb50">
			<div class="col-md-6 col-sm-12 mb-sm-30">
				<p class="mb20">倉ファームは自然豊かな環境の中、水にこだわり昔ながらのスタイルで安心・安全・おいしい野菜を、採れたその日のうちに出荷する農園です。<br>倉ファームは自然豊かな環境の中、水にこだわり昔ながらのスタイルで安心・安全・おいしい野菜を、採れたその日のうちに出荷する農園です。</p>
				<p class="detail_date"><span>収穫時期</span>通年</p>
			</div>
			<div class="col-md-6 col-sm-12">
				<div class="detail_buy">
					<h4 class="detail_buy_title mb10">商品購入はこちらから</h4>
					<ul class="detail_buy_ul">
						<li>
							<p>おまかせパック<br>1箱<br>（ダンボール 25cm × 25cm）</p>
						</li><!--
					 --><li>
							<p class="detail_buy_price"><span>1,890</span>円(税込)</p>
						</li>
					</ul>
					<div class="text-center text-center-xs">
						<a href="" class="detail_buy_btn">ベイスで購入する</a>
						<a href="" class="detail_buy_btn detail_buy_btn_poke">ポケマルで購入する</a>
					</div>
				</div>
			</div>
				</div>
				<div class="text-center text-center-xs">
					<h3 class="detail_feature mb20">特徴・こだわり</h3>
				</div>
				<ul class="ul-3 ul-xs-1 order_net_ul mb30" data-aos="fade-up">
					<li>
						<div class="order_net_ul_img_outer">
							<div class="order_net_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/order_net01.jpg');"></div>
						</div>
						<h4 class="order_net_ul_title mb10">愛情を込めて作っています</h4>
						<p>倉ファームは自然豊かな環境の中、水にこだわり 昔ながらのスタイルで安心・安全・おいしい野菜を、採れたその日のうちに出荷する農園です。</p>
					</li>
					<li>
						<div class="order_net_ul_img_outer">
							<div class="order_net_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/order_net02.jpg');"></div>
						</div>
						<h4 class="order_net_ul_title mb10">愛情を込めて作っています</h4>
						<p>倉ファームは自然豊かな環境の中、水にこだわり 昔ながらのスタイルで安心・安全・おいしい野菜を、採れたその日のうちに出荷する農園です。</p>
					</li>
					<li>
						<div class="order_net_ul_img_outer">
							<div class="order_net_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/order_net03.jpg');"></div>
						</div>
						<h4 class="order_net_ul_title mb10">愛情を込めて作っています</h4>
						<p>倉ファームは自然豊かな環境の中、水にこだわり 昔ながらのスタイルで安心・安全・おいしい野菜を、採れたその日のうちに出荷する農園です。</p>
					</li>
				</ul>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="text-center text-center-xs">
					<h3 class="pt_title03">他の作物を見る</h3>
				</div>
				<ul class="ul-2 ul-xs-1 top_pro_ul mb50" data-aos="fade-up">
					<li>
						<a href="">
							<div class="top_pro_ul_img_outer">
								<div class="top_pro_ul_img_inner">
									<div class="top_pro_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_pro_kiui.jpg');"></div>
								</div>
								<h4 class="top_pro_ul_name"><span>キウイ</span></h4>
								<img class="top_pro_ul_icon" src="<?php echo get_template_directory_uri(); ?>/img/top_pro_icon_kiui.png" alt="キウイ">
							</div>
						</a>
					</li><!--
				 --><li>
						<a href="">
							<div class="top_pro_ul_img_outer">
								<div class="top_pro_ul_img_inner">
									<div class="top_pro_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_pro_atago.jpg');"></div>
								</div>
								<h4 class="top_pro_ul_name"><span>あたご梨</span></h4>
								<img class="top_pro_ul_icon" src="<?php echo get_template_directory_uri(); ?>/img/top_pro_icon.png" alt="あたご梨">
							</div>
						</a>
					</li>
				</ul>
				<div class="text-center text-center-xs"><a href="<?php echo home_url(); ?>/menu" class="pt_btn01">作物の一覧を見る</a></div>
			</div>
		</div>
	</div>	
</section>

</main>






<?php get_footer(); ?>