<?php get_header(); ?>

<?php 
	while ( have_posts() ) : the_post();
?>

<?php 
	// テキスト・ウィジェット関係
	$demand = get_field("demand");
	$advice = get_field("advice");
	$address = get_field("address");
	$period = get_field("period");
	$age = get_field("age");
	$price = get_field("price");
	$item = get_field("item");
	$exam_1_text = get_field("exam_1_text");
	$exam_2_text = get_field("exam_2_text");
	$voice_text = get_field("voice_text");
	$imgs = get_field("imgs");
	// メイン画像
	if ( has_post_thumbnail() ) {
		$image_id = get_post_thumbnail_id();
		$image_url = wp_get_attachment_image_src ($image_id,'large',true);
	} else {
	}  
?>

<main class="pt_bg_dot">
	
<section class="pd-common parallax under_fv" data-parallax-bg-image="<?php echo get_template_directory_uri(); ?>/img/works_fv.jpg" data-parallax-bg-position="center" data-parallax-speed="0.4" data-parallax-direction="down">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<div class="under_fv_txtarea pt_bg_white mt140 mt-xs-80 mb50 pt_br">
					<p class="engTitle h1 mainColor relative">Works</p>
					<h2 class="jpTitle h1 bold">施工事例</h2>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="pd-common">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="h1 jpTitle mainColor bold text-center mb30"><?php the_title(); ?></h3>
				<div class="text-center mb30">
				
                    <?php 
                        if ($terms = get_the_terms($post->ID, 'works_area')) {
                            foreach ( $terms as $term ) {
                                echo '<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">' . esc_html($term->name) . '</p>';
                                $term_slug = $term->slug;
                            }
                        }
                    ?>
				
                    <?php 
                        if ($terms = get_the_terms($post->ID, 'works_cate')) {
                            foreach ( $terms as $term ) {
                                echo '<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">' . esc_html($term->name) . '</p>';
                                $term_slug = $term->slug;
                            }
                        }
                    ?>
				
				</div>
				<div class="detail_main_imgarea">
				
                    <?php if (has_post_thumbnail()):?>
                        <?php 
                            // アイキャッチ画像のIDを取得
                            $thumbnail_id = get_post_thumbnail_id();
                            // mediumサイズの画像内容を取得（引数にmediumをセット）
                            $eye_img = wp_get_attachment_image_src( $thumbnail_id , 'full' );
                            $eye_img_s = wp_get_attachment_image_src( $thumbnail_id , 'thumb_size_s_false',false );
                        ?>
                            <img src="<?php echo $eye_img_s[0];?>" alt="">
                        <?php else: ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/sample01.png" alt="">
                    <?php endif; ?>
				
				</div>
			</div>
		</div>
	</div>
	<div class="container detail_txtarea mb50">
		<div class="row">
			<div class="col-sm-6">
			    <div class="entry">
                    <?php the_content();?>
			    </div>
			</div>
			<div class="col-sm-6">
				<div class="detail_table_outer bgLightBlueColor pt_br">
					<p class="h1 white engTitle text-center mb30 mb-xs-10">Data</p>
					<table class="detail_table white">
						<tbody>
							<tr>
								<th>施工エリア</th>
								<td>
                                    <?php 
                                        if ($terms = get_the_terms($post->ID, 'works_area')) {
                                            foreach ( $terms as $term ) {
                                                echo '<span>' . esc_html($term->name) . '</span>';
                                                $term_slug = $term->slug;
                                            }
                                        }
                                    ?>
								</td>
							<tr>
								<th>施工カテゴリ</th>
								<td>
                                    <?php 
                                        if ($terms = get_the_terms($post->ID, 'works_cate')) {
                                            foreach ( $terms as $term ) {
                                                echo '<span>' . esc_html($term->name) . '</span>';
                                                $term_slug = $term->slug;
                                            }
                                        }
                                    ?>
								</td>
							</tr>
							<tr>
								<th>投稿日</th>
								<td><?php the_time('Y年n月j日'); ?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="container detail_txtarea">
		<div class="row">
			<div class="col-sm-12">
				<p class="detail_before_title h3 numTitle mainColor italic mb10">Before</p>
				<ul class="detail_before_ul ul-4 ul-xs-2">
				
                    <?php $i = 1; ?>
                    <?php while (get_field('before_img_0'.$i)) : ?>
                       
                        <li>
                            <a href="<?php the_field('before_img_0'.$i); ?>" data-lightbox="before" data-title="<?php the_field('before_text_0'.$i); ?>">
                                <div class="detail_before_ul_img_outer">
                                    <div class="detail_before_ul_img bg-common" style="background-image: url('<?php the_field('before_img_0'.$i); ?>');"></div>
                                </div>
                                <p class="detail_before_ul_title bgYellowColor text_m"><?php the_field('before_text_0'.$i); ?></p>
                            </a>
                        </li>
                    <?php $i++; ?>
                    <?php endwhile; ?>
				</ul>
			</div>
			<div class="col-sm-12">
				<p class="detail_before_title h3 numTitle mainColor italic mb10">After</p>
				<ul class="detail_before_ul ul-4 ul-xs-2">
				
                    <?php $i = 1; ?>
                    <?php while (get_field('after_img_0'.$i)) : ?>
                       
                        <li>
                            <a href="<?php the_field('after_img_0'.$i); ?>" data-lightbox="before" data-title="<?php the_field('after_text_0'.$i); ?>">
                                <div class="detail_before_ul_img_outer">
                                    <div class="detail_before_ul_img bg-common" style="background-image: url('<?php the_field('after_img_0'.$i); ?>');"></div>
                                </div>
                                <p class="detail_before_ul_title bgYellowColor text_m"><?php the_field('after_text_0'.$i); ?></p>
                            </a>
                        </li>
                    <?php $i++; ?>
                    <?php endwhile; ?>
				</ul>
			</div>
		</div>
	</div>
</section>
	
<section class="pd-common bgSubColor pt_bg_border">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center">
					<p class="pt_eng_title engTitle h1 mainColor relative">Others</p>
					<h3 class="jpTitle h1 mainColor bold mb50 mb-xs-20">他の事例を見る</h3>
				</div>
				<ul class="top_works_ul ul-3 ul-sm-2 ul-xs-1 mb50 mb-xs-30">
                    <?php
                        $args = array(
                            'post_type' => 'works', //投稿タイプ名
                            'posts_per_page' => 2, //出力する記事の数
                            'tax_query' => array(
                            array(
                            'taxonomy' => 'works_cate', //タクソノミー名
                            'field' => 'slug',
                            'terms' => $term_slug //タームのスラッグ
                            )
                            )
                            );
                        ?>
                        <?php
                        $myposts = get_posts( $args );
                        foreach ( $myposts as $post ) : setup_postdata( $post );
                        ?>

                    <?php get_template_part('content-post-works-archive'); ?>

                    <?php endforeach; ?>
				</ul>
				<div class="text-center">
					<a href="<?php echo home_url(); ?>/works" class="pt_btn bold bgMainColor mainBorderColor">施工事例</a>
				</div>
			</div>
		</div>
	</div>	
</section>

</main>








<?php 
	endwhile;
?>	


<?php get_footer(); ?>


