<?php get_header(); ?>

<main>

<!--<section class="pd-common parallax" data-parallax-bg-image="<?php echo get_template_directory_uri(); ?>/img/top_staff_bg.jpg" data-parallax-bg-position="center" data-parallax-speed="0.4" data-parallax-direction="down">-->
<section class="bgImg topStaffBg" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_staff_bg.jpg');">

	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<div class="under_fv_txtarea pt_bg_white mt140 mt-xs-80 mb50 pt_br">
					<p class="engTitle h1 mainColor relative">Staff</p>
					<h2 class="jpTitle h1 bold">スタッフ紹介</h2>
				</div>
			</div>
		</div>
	</div>
</section>
	
<section class="pd-common bgSubColor pt_bg_border">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center">
					<p class="pt_eng_title engTitle h1 mainColor relative">Message</p>
					<h3 class="jpTitle h1 mainColor bold mb50 mb-xs-20">お客様の想いをカタチに</h3>
				</div>
				<p class="width800 mb50 mb-xs-30">外構とは、住まいを装ったり、守られた空間を作ったり、自然との触れ合いを楽しんだりする、暮らしを取り囲む大切な空間です。だからこそ、ご家族の理想の暮らしをかたちにするためには、敷地条件や間取り、家族のこと、趣味のこと、日々の暮らしのことなど、お客様のことをたくさん知ったうえで外構のご提案をしたいと考えています。</p>
				
				<!-- スタッフ紹介 ここから -->
				<div class="staff_area pt_br bgWhiteColor mb50" data-aos="fade-up">
					<ul class="staff_ul_top ul-2 ul-xs-1 mb20">
						<li>
							<div class="staff_img bg-common pt_br" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/staff01.jpg');"></div>
						</li>
						<li>
							<p class="h3 h3 mainColor bold mb30"><span class="jpTitle">白根 博行</span>　<span class="h5 lightgrayColor">Shirane Hiroyuki</span></p>
							<table class="staff_ul_table mb10">
								<tbody>
									<tr>
										<th><p class="pt_marker inline bold">Q 仕事で大切にしていることは？</p></th>
										<td>A 住んでいる人のことを考えて仕事に向き合い続ける。</td>
									</tr>
								</tbody>
							</table>
							<table class="staff_ul_table mb10">
								<tbody>
									<tr>
										<th><p class="pt_marker inline bold">Q 仕事のやりがいは？</p></th>
										<td>A 自分のした仕事が形に残ること。</td>
									</tr>
								</tbody>
							</table>
							<table class="staff_ul_table mb10">
								<tbody>
									<tr>
										<th><p class="pt_marker inline bold">Q 休日の過ごし方は？</p></th>
										<td>A お笑い系を中心にテレビ鑑賞しています。</td>
									</tr>
								</tbody>
							</table>
						</li>
					</ul>
				</div>
				<!-- スタッフ紹介 ここまで -->
				
				<!-- スタッフ紹介 ここから -->
				<div class="staff_area pt_br bgWhiteColor mb50" data-aos="fade-up">
					<ul class="staff_ul_top ul-2 ul-xs-1 mb20">
						<li>
							<p class="h3 h3 mainColor bold mb30"><span class="jpTitle">河元 未来</span>　<span class="h5 lightgrayColor">Komoto Mirai</span></p>
							<table class="staff_ul_table mb10">
								<tbody>
									<tr>
										<th><p class="pt_marker inline bold">Q この仕事で嬉しさを感じる瞬間は？</p></th>
										<td>A 完成した現場を見て、お客様が喜んでくれる顔を見た時。</td>
									</tr>
								</tbody>
							</table>
							<table class="staff_ul_table mb10">
								<tbody>
									<tr>
										<th><p class="pt_marker inline bold">Q 仕事で大切にしていることは？</p></th>
										<td>A 仲間と助け合って、一緒に仕事に取り組むこと。</td>
									</tr>
								</tbody>
							</table>
							<table class="staff_ul_table mb10">
								<tbody>
									<tr>
										<th><p class="pt_marker inline bold">Q 趣味は？</p></th>
										<td>A 読書と映画。主に推理小説と洋画アクションが好きです。</td>
									</tr>
								</tbody>
							</table>
						</li>
						<li>
							<div class="staff_img bg-common pt_br" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/staff02.jpg');"></div>
						</li>
					</ul>
				</div>
				<!--
				<div class="staff_area pt_br bgWhiteColor mb50" data-aos="fade-up">
					<ul class="staff_ul_top ul-2 ul-xs-1 mb20">
						<li>
							<h4 class="jpTitle h1 mainColor bold mb10">お客様の笑顔のために</h4>
							<p class="mb10">外構とは、住まいを装ったり、守られた空間を作ったり、自然との触れ合いを楽しんだりする、暮らしを取り囲む大切な空間です。だからこそ、ご家族の理想の暮らしをかたちにするためには、敷地条件や間取り、家族のこと、趣味のこと、日々の暮らしのことなど、お客様のことをたくさん知ったうえで外構のご提案をしたいと考えています。</p>
							<p class="h3 h3 mainColor bold"><span class="jpTitle">田中 一郎</span>　<span class="h5 lightgrayColor">Tanaka Ichiro</span></p>
							<p class="jpTitle">エクステリアコーディネーター</p>
						</li>
						<li>
							<div class="staff_img bg-common pt_br" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/staff02.jpg');"></div>
						</li>
					</ul>
					<ul class="staff_ul_bottom ul-2 ul-xs-1">
						<li class="matchHeight">
							<table class="staff_ul_table">
								<tbody>
									<tr>
										<th>
											<p class="pt_marker inline_block mb20">仕事でのマストアイテム</p>
											<p class="bold">◯◯のノート</p>
											<p>使いやすさが抜群！</p>
										</th>
										<td>
											<div class="staff_ul_img bg-common pt_br" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/staff_item01.jpg');"></div>
										</td>
									</tr>
								</tbody>
							</table>
						</li>
						<li class="matchHeight">
							<table class="staff_ul_table">
								<tbody>
									<tr>
										<th>
											<p class="pt_marker inline_block mb0">好きなこと・</p>
											<p class="pt_marker inline_block">ハマっていること</p>
										</th>
										<td class="mb-xs-10">・写真を撮ること<br>・ビール工場巡り</td>
									</tr>
									<tr>
										<th><p class="pt_marker inline_block">仕事でのこだわり</p></th>
										<td>お客様や理想やビジョンを最大限に優先し、予算や工数との折り合いをつけること</td>
									</tr>
								</tbody>
							</table>
						</li>
					</ul>
				</div>
				-->
				<!-- スタッフ紹介 ここまで -->
				
				<!-- スタッフ紹介 ここから -->
				<div class="staff_area pt_br bgWhiteColor mb50" data-aos="fade-up">
					<ul class="staff_ul_top ul-2 ul-xs-1 mb20">
						<li>
							<div class="staff_img bg-common pt_br" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/staff03.jpg');"></div>
						</li>
						<li>
							<p class="h3 h3 mainColor bold mb30"><span class="jpTitle">松本 美奈</span>　<span class="h5 lightgrayColor">Matsumoto Mina</span></p>
							<table class="staff_ul_table mb10">
								<tbody>
									<tr>
										<th><p class="pt_marker inline bold">Q この仕事の魅力は？</p></th>
										<td>A 仕事を通して色んな人たちと出会え、色んな場所に行けること</td>
									</tr>
								</tbody>
							</table>
							<table class="staff_ul_table mb10">
								<tbody>
									<tr>
										<th><p class="pt_marker inline bold">Q 仕事で意識していることは？</p></th>
										<td>A 仕事が終わった後の片付け。キレイな状態でお客様にお渡ししたい。</td>
									</tr>
								</tbody>
							</table>
							<table class="staff_ul_table mb10">
								<tbody>
									<tr>
										<th><p class="pt_marker inline bold">Q 休日の過ごし方は？</p></th>
										<td>A 車好きなので洗車してます。</td>
									</tr>
								</tbody>
							</table>
						</li>
					</ul>
				</div>
				<!-- スタッフ紹介 ここまで -->
				
				<!-- スタッフ紹介 ここから -->
				<div class="staff_area pt_br bgWhiteColor mb50" data-aos="fade-up">
					<ul class="staff_ul_top ul-2 ul-xs-1 mb20">
						<li>
							<p class="h3 h3 mainColor bold mb30"><span class="jpTitle">高塚 貫一</span>　<span class="h5 lightgrayColor">Takatsuka Kannchi</span></p>
							<table class="staff_ul_table mb10">
								<tbody>
									<tr>
										<th><p class="pt_marker inline bold">Q 仕事で意識していることは？</p></th>
										<td>A 我が家を施工している気持ちで仕事をすること。</td>
									</tr>
								</tbody>
							</table>
							<table class="staff_ul_table mb10">
								<tbody>
									<tr>
										<th><p class="pt_marker inline bold">Q 仕事で大切にしていることは？</p></th>
										<td>A 自分が納得できる仕事をすること。お天道様は必ず見てます。</td>
									</tr>
								</tbody>
							</table>
							<table class="staff_ul_table mb10">
								<tbody>
									<tr>
										<th><p class="pt_marker inline bold">Q 趣味は？</p></th>
										<td>A 詩吟とピアノです。</td>
									</tr>
								</tbody>
							</table>
						</li>
						<li>
							<div class="staff_img bg-common pt_br" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/staff04.jpg');"></div>
						</li>
					</ul>
				</div>
				<!-- スタッフ紹介 ここまで -->
			</div>
		</div>
	</div>	
</section>

<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>