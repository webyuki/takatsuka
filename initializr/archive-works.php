<?php get_header(); ?>


<main class="pt_bg_dot">
	
<section class="pd-common parallax" data-parallax-bg-image="<?php echo get_template_directory_uri(); ?>/img/works_fv.jpg" data-parallax-bg-position="center" data-parallax-speed="0.4" data-parallax-direction="down">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<div class="under_fv_txtarea pt_bg_white mt140 mt-xs-80 mb50 pt_br">
					<p class="engTitle h1 mainColor relative">Works</p>
					<h2 class="jpTitle h1 bold">施工事例</h2>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="pd-common">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<ul class="list_link mb50">
								
					<li>
						<a href="<?php echo home_url();?>/works" class="yellowBorderColor">全て</a>
					</li>
                    <?php $categories = get_categories(array('taxonomy' => 'works_cate')); if ( $categories ) : ?>
                        <?php foreach ( $categories as $category ): ?>
                            <li><a class="yellowBorderColor" href="<?php echo home_url();?>/works_cate/<?php echo esc_html( $category->slug);?>"><?php echo wp_specialchars( $category->name ); ?></a></li>
                        <?php endforeach; ?>
                    <?php endif; ?>
				</ul>
				<ul class="top_works_ul ul-3 ul-sm-2 ul-xs-1 mb50">
                    <?php			
                        while ( have_posts() ) : the_post();
                            get_template_part('content-post-works-archive'); 
                        endwhile;
                    ?>
				</ul>
			</div>
		</div>
		<?php get_template_part( 'parts/pagenation' ); ?>
	</div>
</section>

</main>




<?php get_footer(); ?>