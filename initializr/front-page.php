<?php get_header(); ?>

<main class="">

<!-- ファーストビュー ここから -->
<section class="relative topFvSection mb50 mb-xs-0">
	<div class="topFv">
		<div class="main_imgBox bgSubColor">
			<div class="main_img" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv01.jpg');"></div>
			<div class="main_img" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv02.jpg');"></div>
			<div class="main_img" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv03.jpg');"></div>
			<div class="main_img" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv04.jpg');"></div>
		</div>
	</div>
	<div class="topFvBoxWrap" data-aos="fade-up">
		<img src="<?php echo get_template_directory_uri();?>/img/fv_txt.png" alt="幸せ育むエクステリアで、毎日を楽しく、豊かに">
	</div>
</section>
<!-- ファーストビュー ここまで -->
	
	
<section class="pd-common pt_bg_dot">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center">
					<p class="pt_eng_title engTitle h1 mainColor relative">Concept</p>
					<h3 class="jpTitle h1 mainColor bold mb50 mb-xs-20">迎える・楽しむ・彩る<br>エクステリアが作る家族の暮らし。</h3>
				</div>
				<div class="width720">
<p>髙塚ではご家族が安心して住まい、季節の移ろいを存分に楽しめるエクステリアを施工しています。</p>
<p>サービスの柱となるのは、お客さまのご要望にあったプランのご提案と、熟練した職人の高品質な技術。</p>
<p>「いってらっしゃい」と「おかえりなさい」、そして「ようこそ」を届けるために。</p>
<p>住まう人をあたたかく見守り、毎日を支える庭を作ります。</p>
                
                </div>


			</div>
		</div>
	</div>	
</section>
	 
<section class="pd-common parallax" data-parallax-bg-image="<?php echo get_template_directory_uri(); ?>/img/top_concept_bg.jpg" data-parallax-bg-position="center bottom" data-parallax-speed="0.4" data-parallax-direction="down">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<ul class="top_concept_ul ul-3 ul-xs-1 mb40">
					<li>
						<div class="top_concept_ul_inner pt_bg_white pt_br matchHeight" data-aos="fade-up">		
							<p class="numTitle h0 semibold mainColor italic text-center mb10">01</p>
							<div class="top_concept_img_outer w100 mb20">
								<div class="pt_circle bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_concept01.jpg');"></div>
							</div>
							<h4 class="jpTitle h3 bold mainColor text-center mb20">オーダーメイドの<br>プラン作成</h4>
							<p>ご予算とご希望に合わせた完全オーダーメイドのプランをご提案し、高い技術で誠実な施工をしています。</p>
						</div>
					</li>
					<li>
						<div class="top_concept_ul_inner pt_bg_white pt_br matchHeight" data-aos="fade-up">		
							<p class="numTitle h0 semibold mainColor italic text-center mb10">02</p>
							<div class="top_concept_img_outer w100 mb20">
								<div class="pt_circle bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_concept02.jpg');"></div>
							</div>
							<h4 class="jpTitle h3 bold mainColor text-center mb20">クレームなし、<br>アフター不要の施工</h4>
							<p>クレームやアフターメンテナンスの必要がほとんどない確実な施工が、お客さまから高く評価されています。</p>
						</div>
					</li>
					<li>
						<div class="top_concept_ul_inner pt_bg_white pt_br matchHeight" data-aos="fade-up">		
							<p class="numTitle h0 semibold mainColor italic text-center mb10">03</p>
							<div class="top_concept_img_outer w100 mb20">
								<div class="pt_circle bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_concept03.jpg');"></div>
							</div>
							<h4 class="jpTitle h3 bold mainColor text-center mb20">地域に密着した<br>スピーディーな対応</h4>
							<p>業者やメーカーとの連携を深め、お客さまからの「ちょっと見てほしい」にスピーディーに対応しています。</p>
						</div>
					</li>
				</ul>
				<div class="text-center">
					<a href="<?php echo home_url(); ?>/about-us" class="pt_btn bold bgMainColor mainBorderColor">高塚の想い</a>
				</div>
			</div>
		</div>
	</div>
</section>
	
<section class="pd-common bgSubColor pt_bg_border">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center">
					<p class="pt_eng_title engTitle h1 mainColor relative">Service</p>
					<h3 class="jpTitle h1 mainColor bold mb50 mb-xs-20">お庭のお悩み、<br>髙塚が解決します。</h3>
				</div>
				<p class="width720 mb50">お庭まわりのお悩みは、髙塚におまかせください。ご予算とご希望に応じて、季節を彩る素敵なエクステリアをご提案します。</p>
			</div>
		</div>
	</div>
	<ul class="top_service_ul ul-4 ul-xs-1" data-aos="fade-up">
		<li>
			<a href="<?php echo home_url(); ?>/service#service01">
				<div class="top_service_img bg-common relative" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_service01.jpg');">
					<h4 class="top_service_title jpTitle h3 bold mainColor text-center pt_bg_white pt_br_s">新築外構の施工</h4>
				</div>
			</a>
		</li>
		<li>
			<a href="<?php echo home_url(); ?>/service#service02">
				<div class="top_service_img bg-common relative" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_service02.jpg');">
					<h4 class="top_service_title jpTitle h3 bold mainColor text-center pt_bg_white pt_br_s">庭のリフォーム</h4>
				</div>
			</a>
		</li>
		<li>
			<a href="<?php echo home_url(); ?>/service#service03">
				<div class="top_service_img bg-common relative" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_service03.jpg');">
					<h4 class="top_service_title jpTitle h3 bold mainColor text-center pt_bg_white pt_br_s">プライバシーの確保</h4>
				</div>
			</a>
		</li>
		<li>
			<a href="<?php echo home_url(); ?>/service#service04">
				<div class="top_service_img bg-common relative" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_service04.jpg');">
					<h4 class="top_service_title jpTitle h3 bold mainColor text-center pt_bg_white pt_br_s">お庭の雑草対策</h4>
				</div>
			</a>
		</li>
	</ul>
	<ul class="top_service_ul ul-3 ul-xs-1 mb50" data-aos="fade-up">
		<li>
			<a href="<?php echo home_url(); ?>/service#service04">
				<div class="top_service_img bg-common relative" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_service05.jpg');">
					<h4 class="top_service_title jpTitle h3 bold mainColor text-center pt_bg_white pt_br_s">カーポートの設置</h4>
				</div>
			</a>
		</li>
		<li>
			<a href="<?php echo home_url(); ?>/service#service04">
				<div class="top_service_img bg-common relative" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_service06.jpg');">
					<h4 class="top_service_title jpTitle h3 bold mainColor text-center pt_bg_white pt_br_s">境界ブロックの設置</h4>
				</div>
			</a>
		</li>
		<li>
			<a href="<?php echo home_url(); ?>/service#service04">
				<div class="top_service_img bg-common relative" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_service07.jpg');">
					<h4 class="top_service_title jpTitle h3 bold mainColor text-center pt_bg_white pt_br_s">お墓まわりの修繕</h4>
				</div>
			</a>
		</li>
	</ul>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center">
					<a href="<?php echo home_url(); ?>/service" class="pt_btn bold bgMainColor mainBorderColor mb20">施工プラン</a>
				</div>
			</div>
		</div>
	</div>
</section>
	
	
<section class="pd-common pt_bg_dot">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center">
					<p class="pt_eng_title engTitle h1 mainColor relative">Works</p>
					<h3 class="jpTitle h1 mainColor bold mb50 mb-xs-20">施工事例</h3>
				</div>
				<ul class="top_works_ul ul-3 ul-sm-2 ul-xs-1 mb50 mb-xs-30" data-aos="fade-up">
				
                    <?php
                        $args = array(
                            'post_type' => 'works', //投稿タイプ名
                            'posts_per_page' => 6 //出力する記事の数
                        );
                    ?>
                    <?php
                        $myposts = get_posts( $args );
                        foreach ( $myposts as $post ) : setup_postdata( $post );
                    ?>

                    <?php get_template_part('content-post-works-archive'); ?>

                    <?php endforeach; ?>
				</ul>
				<div class="text-center">
					<a href="<?php echo home_url(); ?>/works" class="pt_btn bold bgMainColor mainBorderColor">施工事例</a>
				</div>
			</div>
		</div>
	</div>	
</section>

<!--
<section class="pd-common parallax" data-parallax-bg-image="<?php echo get_template_directory_uri(); ?>/img/top_staff_bg.jpg" data-parallax-bg-position="center bottom" data-parallax-speed="0.4" data-parallax-direction="down">
-->
<section class="bgImg topStaffBg" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_staff_bg.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<div class="under_fv_txtarea pt_bg_white mt50 mb50 mt-xs-20 mb-xs-20 pt_br">
					<p class="engTitle h1 mainColor relative">Staff</p>
					<h3 class="jpTitle h1 mainColor bold">スタッフ紹介</h3>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="pd-common bgWhiteColor">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center mb30">
					<h4 class="jpTitle pt_marker h3 bold inline_block mb40">お客様の想いをカタチに</h4>
					<p class="width720 text-left-xs">外構とは、住まいを装ったり、守られた空間を作ったり、自然との触れ合いを楽しんだりする、暮らしを取り囲む大切な空間です。だからこそ、ご家族の理想の暮らしをかたちにするためには、敷地条件や間取り、家族のこと、趣味のこと、日々の暮らしのことなど、お客様のことをたくさん知ったうえで外構のご提案をしたいと考えています。</p>
				</div>
			</div>
		</div>
	</div>
	<ul class="top_staff_ul ul-3 ul-xs-1 mb50 mb-xs-30">
		<li>
			<div class="top_staff_img_outer" data-aos="fade-right">
				<div class="top_staff_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_staff01.jpg');"></div>
			</div>
		</li>
		<li>
			<div class="top_staff_img_outer" data-aos="fade-up">
				<div class="top_staff_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_staff02.jpg');"></div>
			</div>
		</li>
		<li>
			<div class="top_staff_img_outer" data-aos="fade-left">
				<div class="top_staff_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_staff03.jpg');"></div>
			</div>
		</li>
	</ul>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center">
					<a href="<?php echo home_url(); ?>/staff" class="pt_btn bold bgMainColor mainBorderColor">スタッフ紹介</a>
				</div>
			</div>
		</div>
	</div>
</section>
	
<section class="pd-common bgSubColor pt_bg_border">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center">
					<p class="pt_eng_title engTitle h1 mainColor relative">Flow</p>
					<h3 class="jpTitle h1 mainColor bold mb50 mb-xs-20">施工の流れ</h3>
				</div>
				<div class="width980">
					<div class="top_flow_area bgWhiteColor pt_br mb20">
						<ul class="top_flow_ul">
							<li>
								<div class="pt_circle bg-common relative" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_flow01.jpg');">
									<span class="top_flow_num numTitle h0 mainColor italic absolute">01</span>
								</div>
							</li><!--
						 --><li>
								<h4 class="jpTitle h3 bold mainColor mb10 text-center-xs">お問合せ</h4>
								<p>ホームページのお問合せフォーム、もしくはお電話にてご連絡ください。工事に関するご要望をお伺いし、ご希望があれば現地調査の日時をご相談させていただきます。</p>
							</li>
						</ul>
					</div>
					<div class="top_flow_area bgWhiteColor pt_br mb20">
						<ul class="top_flow_ul">
							<li>
								<div class="pt_circle bg-common relative" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_flow02.jpg');">
									<span class="top_flow_num numTitle h0 mainColor italic absolute">02</span>
								</div>
							</li><!--
						 --><li>
								<h4 class="jpTitle h3 bold mainColor mb10 text-center-xs">現地調査・ご相談</h4>
								<p>実際の現場を拝見し、お客さまのご希望を詳しくお伺いします。</p>
							</li>
						</ul>
					</div>
					<div class="top_flow_area bgWhiteColor pt_br mb20">
						<ul class="top_flow_ul">
							<li>
								<div class="pt_circle bg-common relative" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_flow03.jpg');">
									<span class="top_flow_num numTitle h0 mainColor italic absolute">03</span>
								</div>
							</li><!--
						 --><li>
								<h4 class="jpTitle h3 bold mainColor mb10 text-center-xs">施工プランのご提案</h4>
								<p>現地調査とヒアリングの内容をもとに、１～２週間で施工プランとお見積を作成します。</p>
							</li>
						</ul>
					</div>
					<div class="top_flow_area bgWhiteColor pt_br mb20">
						<ul class="top_flow_ul">
							<li>
								<div class="pt_circle bg-common relative" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_flow04.jpg');">
									<span class="top_flow_num numTitle h0 mainColor italic absolute">04</span>
								</div>
							</li><!--
						 --><li>
								<h4 class="jpTitle h3 bold mainColor mb10 text-center-xs">ご契約</h4>
								<p>予算とプラン内容をご確認いただき、お打ち合わせ後にご了承いただいた内容でご契約を締結します。</p>
							</li>
						</ul>
					</div>
					<div class="top_flow_area bgWhiteColor pt_br mb20">
						<ul class="top_flow_ul">
							<li>
								<div class="pt_circle bg-common relative" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_flow05.jpg');">
									<span class="top_flow_num numTitle h0 mainColor italic absolute">05</span>
								</div>
							</li><!--
						 --><li>
								<h4 class="jpTitle h3 bold mainColor mb10 text-center-xs">着工</h4>
								<p>資材の発注・手配や職人との打ち合わせを行い、着工します。施工中にお気付きの点ございましたら、いつでもお知らせください。</p>
							</li>
						</ul>
					</div>
					<div class="top_flow_area bgWhiteColor pt_br mb20">
						<ul class="top_flow_ul">
							<li>
								<div class="pt_circle bg-common relative" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_flow06.jpg');">
									<span class="top_flow_num numTitle h0 mainColor italic absolute">06</span>
								</div>
							</li><!--
						 --><li>
								<h4 class="jpTitle h3 bold mainColor mb10 text-center-xs">竣工・お引渡し</h4>
								<p>施工後、管理者が最終チェックを行い、お客様にご確認いただいた後、問題がなければお引渡し完了です。</p>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>	
</section>
	
<section class="pd-common bgWhiteColor">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center">
					<p class="pt_eng_title engTitle h1 mainColor relative">Q&amp;A</p>
					<h3 class="jpTitle h1 mainColor bold mb50 mb-xs-20">よくあるご質問</h3>
				</div>
				<ul class="ul-2 ul-sm-1 top_qa_ul">
					<li>
						<p class="top_qa_txt top_qa_txt_q">Q. 見積は無料ですか？</p>
						<p class="top_qa_txt top_qa_txt_a">A. はい、お見積は無料です。お気軽にお問い合わせください。</p>
					</li>
					<li>
						<p class="top_qa_txt top_qa_txt_q">Q. 費用はどのくらいかかりますか？</p>
						<p class="top_qa_txt top_qa_txt_a">A. 工事にかかる費用は、規模や設備によって異なります。ご予算のご相談から承りますので、お客さまのご希望をお聞かせください。</p>
					</li>
					<li>
						<p class="top_qa_txt top_qa_txt_q">Q. 規模の大きな工事はできますか？</p>
						<p class="top_qa_txt top_qa_txt_a">A. 可能です。弊社では施設の外構や数百メートル規模のフェンスの設置など、規模の大きな工事にも対応しています。</p>
					</li>
					<li>
						<p class="top_qa_txt top_qa_txt_q">Q. 他社で施工したカーポートの修理はできますか？</p>
						<p class="top_qa_txt top_qa_txt_a">A. 可能です。まずは現場を拝見しますので、ご都合のよい日をお知らせください。</p>
					</li>
					<li>
						<p class="top_qa_txt top_qa_txt_q">Q. 購入した土地から大きな石が出てきました。処分してもらえますか？</p>
						<p class="top_qa_txt top_qa_txt_a">A. はい、弊社では石や残土の処理や移動も承っています。お気軽にご相談ください。</p>
					</li>
					<li>
						<p class="top_qa_txt top_qa_txt_q">Q. 家庭菜園を始めたいのですが、土が足りません。</p>
						<p class="top_qa_txt top_qa_txt_a">A. 菜園に適した土を搬入します。土地の広さなどを拝見してご相談させていただきますので、まずはご連絡ください。</p>
					</li>
					<li>
						<p class="top_qa_txt top_qa_txt_q">Q. お墓の上り口が崩れてきました。修理をお願いできますか？</p>
						<p class="top_qa_txt top_qa_txt_a">A. はい、承ります。必要があれば石材店との連携も可能ですので、ご要望があればお気軽にお聞かせください。</p>
					</li>
				</ul>
				<script>
					var $ = jQuery;
					$(function(){
						$(".top_qa_txt_q").on("click", function() {
							$(this).next().slideToggle();
						});
					});
				</script>
			</div>
		</div>
	</div>
</section>
	
<section class="pd-common bgSubColor pt_bg_border" id="top_news">
	<div class="container mt50 mb50 mt-xs-20 mb-xs-20">
		<div class="row">
			<div class="col-sm-4">
				<div class="text-center-xs">
					<p class="engTitle h1 mainColor relative">News</p>
					<h3 class="jpTitle h1 mainColor bold mb50 mb-xs-20">新着情報</h3>
				</div>
				<div class="mb20 mb-xs-50 text-center-xs hidden-xs"><a href="<?php echo home_url(); ?>/news" class="pt_btn bold bgMainColor mainBorderColor text-center">過去の新着情報</a></div>
			</div>
			<div class="col-sm-8">
				<div class="top_news_area">
					<ul class="top_news_ul mb-xs-50">
                       
                        <?php
                            //$paged = (get_query_var('page')) ? get_query_var('page') : 1;
                            $paged = get_query_var('page');
                            $args = array(
                                'post_type' =>  'post', // 投稿タイプを指定
                                'paged' => $paged,
                                'posts_per_page' => 3, // 表示するページ数
                                'orderby'=>'date',
                                /*'cat' => -4,*/
                                'order'=>'DESC'
                                        );
                            $wp_query = new WP_Query( $args ); // クエリの指定 	
                            while ( $wp_query->have_posts() ) : $wp_query->the_post();
                                //ここに表示するタイトルやコンテンツなどを指定 
                            get_template_part('content-post-top'); 
                            endwhile;
                            //wp_reset_postdata(); //忘れずにリセットする必要がある
                            wp_reset_query();
                        ?>		
                       
					</ul>
				</div>
				<div class="text-center-xs visible-xs"><a href="<?php echo home_url(); ?>/news" class="pt_btn bold bgMainColor mainBorderColor">過去の新着情報</a></div>
			</div>
		</div>
	</div>	
</section>
	
</main>

<?php get_footer(); ?>