<?php get_header(); ?>
<main>

<?php
	$category = get_the_category();
	$cat_id   = $category[0]->cat_ID;
	$cat_name = $category[0]->cat_name;
	$cat_slug = $category[0]->category_nicename;
?>

<!-- カテゴリーIDを表示したい所に -->
<?php //echo $cat_id; ?>

<!-- カテゴリー名を表示したい所に -->
<?php //echo $cat_name; ?>

<!-- カテゴリースラッグを表示したい所に -->
<?php //echo $cat_slug; ?>




<main>

<section class="pd-common parallax" data-parallax-bg-image="<?php echo get_template_directory_uri(); ?>/img/about_fv.jpg" data-parallax-bg-position="center" data-parallax-speed="0.4" data-parallax-direction="down">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<div class="under_fv_txtarea pt_bg_white mt140 mt-xs-80 mb50 pt_br">
					<p class="engTitle h1 mainColor relative">News</p>
					<h2 class="jpTitle h1 bold"><?php echo get_the_archive_title(); ?></h2>
				</div>
			</div>
		</div>
	</div>
</section>



<section class="pd-common relative paperBgUnder">
	<div class="container">
		<?php //get_template_part( 'parts/breadcrumb' ); ?>				
		<div class="row">
			<div class="col-sm-9">
				<?php
					while ( have_posts() ) : the_post();
						get_template_part('content-post'); 
					endwhile;
				?>
			</div>
			<div class="col-sm-3">
				<?php dynamic_sidebar(); ?>
			</div>
		</div>
		<?php get_template_part( 'parts/pagenation' ); ?>
	</div>
</section>

</main>


<?php get_footer(); ?>