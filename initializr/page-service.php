<?php get_header(); ?>

<main class="pt_bg_dot">

<section class="pd-common parallax mb50" data-parallax-bg-image="<?php echo get_template_directory_uri(); ?>/img/service_fv.jpg" data-parallax-bg-position="center" data-parallax-speed="0.4" data-parallax-direction="down">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<div class="under_fv_txtarea pt_bg_white mt140 mt-xs-80 mb50 pt_br">
					<p class="engTitle h1 mainColor relative">Service</p>
					<h2 class="jpTitle h1 bold">施工プラン</h2>
				</div>
			</div>
		</div>
	</div>
</section>
	
<section id="service01" class="pd-common parallax" data-parallax-bg-image="<?php echo get_template_directory_uri(); ?>/img/service_bg01.jpg" data-parallax-bg-position="center" data-parallax-speed="0.4" data-parallax-direction="down">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<div class="under_fv_txtarea pt_bg_white mt50 mb50 mt-xs-20 mb-xs-20 pt_br">
					<p class="numTitle h0 semibold mainColor italic text-center mb0">01</p>
					<h3 class="jpTitle h1 mainColor bold">新築外構の施工</h3>
				</div>
			</div>
		</div>
	</div>
</section>
	
<section class="pd-common">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center">
					<h4 class="jpTitle pt_marker h3 bold inline_block mb40">予算や希望を叶えるプランをご提案</h4>
				</div>
				<p class="width800 mb50">お住いの新築時、外構は「メーカーから提案されたプランに不満があった」、「建物のみの施工で、外構は別注だった」など、建物の竣工にあわせ、お客さまご自身がゼロから考えなければならない場合があります。髙塚では、「予算を抑えたい」や「好みの庭にしたい」など、お客さまの希望に沿ったご提案をしています。小さな工事でも、お気軽にご相談ください。</p>
				<div class="service_warning_area pt_br bgSubColor mb50 width800">
					<h4 class="jpTitle h3 mainColor bold mb30 mb-xs-20 text-center">こんな工事をしています</h4>
					<div class="service_check_list">
						<p class="pt_check_title h4 bold mb10 mb-xs-10">カーポートの施工</p>
						<p class="pt_check_title h4 bold mb10 mb-xs-10">玄関アプローチの施工</p>
						<p class="pt_check_title h4 bold mb10 mb-xs-10">植栽</p>
						<p class="pt_check_title h4 bold mb10 mb-xs-10">門柱・ポスト・表札・照明などの施工</p>
						<p class="pt_check_title h4 bold mb10 mb-xs-10">境界ブロックの設置</p>
						<p class="pt_check_title h4 bold mb10 mb-xs-10">雑草対策</p>
					</div>
				</div>
                 <!--                
				<h4 class="jpTitle h3 mainColor bold mb20 mb-xs-10 text-center">新築外構の施工の料金事例</h4>
				<ul class="top_works_ul ul-3 ul-sm-2 ul-xs-1 mb50" data-aos="fade-up">
					<li>
						<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
							<div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
							<h5 class="h3 jpTitle mainColor bold text-center mb10">新築外構施工</h5>
							<div class="service_example_checkarea">
								<p class="text_m service_example_check">カーポート設置</p>
								<p class="text_m service_example_check">カーポート設置</p>
								<p class="text_m service_example_check mb0">カーポート設置</p>
							</div>
							<div class="text-center">
								<p class="h4 pt_marker inline_block mb20"><span class="h0 lh_s semibold numTitle italic">58</span>万円（税抜）</p>
								<p class="text_m text-center">※屋根坪20坪・切妻屋根66㎡</p>
							</div>
						</div>
					</li>
					<li>
						<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
							<div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
							<h5 class="h3 jpTitle mainColor bold text-center mb10">新築外構施工</h5>
							<div class="service_example_checkarea">
								<p class="text_m service_example_check">カーポート設置</p>
								<p class="text_m service_example_check">カーポート設置</p>
								<p class="text_m service_example_check mb0">カーポート設置</p>
							</div>
							<div class="text-center">
								<p class="h4 pt_marker inline_block mb20"><span class="h0 lh_s semibold numTitle italic">58</span>万円（税抜）</p>
								<p class="text_m text-center">※屋根坪20坪・切妻屋根66㎡</p>
							</div>
						</div>
					</li>
					<li>
						<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
							<div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
							<h5 class="h3 jpTitle mainColor bold text-center mb10">新築外構施工</h5>
							<div class="service_example_checkarea">
								<p class="text_m service_example_check">カーポート設置</p>
								<p class="text_m service_example_check">カーポート設置</p>
								<p class="text_m service_example_check mb0">カーポート設置</p>
							</div>
							<div class="text-center">
								<p class="h4 pt_marker inline_block mb20"><span class="h0 lh_s semibold numTitle italic">58</span>万円（税抜）</p>
								<p class="text_m text-center">※屋根坪20坪・切妻屋根66㎡</p>
							</div>
						</div>
					</li>
				</ul>
				-->
			</div>
		</div>
	</div>	
</section>

<section id="service02" class="pd-common parallax" data-parallax-bg-image="<?php echo get_template_directory_uri(); ?>/img/service_bg02.jpg" data-parallax-bg-position="center" data-parallax-speed="0.4" data-parallax-direction="down">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<div class="under_fv_txtarea pt_bg_white mt50 mb50 mt-xs-20 mb-xs-20 pt_br">
					<p class="numTitle h0 semibold mainColor italic text-center mb0">02</p>
					<h3 class="jpTitle h1 mainColor bold">庭のリフォーム</h3>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="pd-common">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center">
					<h4 class="jpTitle pt_marker h3 bold inline_block mb40">リフォームも修繕も、思いのまま</h4>
				</div>
				<p class="width800 mb50">日差しや雨風、外からの思いがけないダメージにさらされ、外構に使用される設備は室内以上に早く劣化します。髙塚ではお住いの外観を長く美しく保つため、小さな修繕から大がかりなリフォームまで、お庭にかかわる工事全般を承っています。他社で施工した設備の修理も可能です。お困りの際はお気軽にご連絡ください。</p>
				<div class="service_warning_area pt_br bgSubColor mb50 width800">
					<h4 class="jpTitle h3 mainColor bold mb30 mb-xs-20 text-center">こんな工事をしています</h4>
					<div class="service_check_list">
						<p class="pt_check_title h4 bold mb10 mb-xs-10">カーポートの施工、拡張、修繕、リフォーム</p>
						<p class="pt_check_title h4 bold mb10 mb-xs-10">門柱・ポスト・表札・照明などの施工、修繕</p>
						<p class="pt_check_title h4 bold mb10 mb-xs-10">外構のバリアフリー工事</p>
						<p class="pt_check_title h4 bold mb10 mb-xs-10">庭のレイアウトの変更</p>
						<p class="pt_check_title h4 bold mb10 mb-xs-10">庭木の撤去</p>
						<p class="pt_check_title h4 bold mb10 mb-xs-10">土の入替、撤去</p>
						<p class="pt_check_title h4 bold mb10 mb-xs-10">雑草対策</p>
						<p class="pt_check_title h4 bold mb10 mb-xs-10">境界ブロックの設置、修繕</p>
					</div>
				</div>                
				<!--
				<h4 class="jpTitle h3 mainColor bold mb20 mb-xs-10 text-center">外構のリフォームの料金事例</h4>
				<ul class="top_works_ul ul-3 ul-sm-2 ul-xs-1 mb50" data-aos="fade-up">
					<li>
						<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
							<div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
							<h5 class="h3 jpTitle mainColor bold text-center mb10">新築外構施工</h5>
							<div class="service_example_checkarea">
								<p class="text_m service_example_check">カーポート設置</p>
								<p class="text_m service_example_check">カーポート設置</p>
								<p class="text_m service_example_check mb0">カーポート設置</p>
							</div>
							<div class="text-center">
								<p class="h4 pt_marker inline_block mb20"><span class="h0 lh_s semibold numTitle italic">58</span>万円（税抜）</p>
								<p class="text_m text-center">※屋根坪20坪・切妻屋根66㎡</p>
							</div>
						</div>
					</li>
					<li>
						<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
							<div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
							<h5 class="h3 jpTitle mainColor bold text-center mb10">新築外構施工</h5>
							<div class="service_example_checkarea">
								<p class="text_m service_example_check">カーポート設置</p>
								<p class="text_m service_example_check">カーポート設置</p>
								<p class="text_m service_example_check mb0">カーポート設置</p>
							</div>
							<div class="text-center">
								<p class="h4 pt_marker inline_block mb20"><span class="h0 lh_s semibold numTitle italic">58</span>万円（税抜）</p>
								<p class="text_m text-center">※屋根坪20坪・切妻屋根66㎡</p>
							</div>
						</div>
					</li>
					<li>
						<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
							<div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
							<h5 class="h3 jpTitle mainColor bold text-center mb10">新築外構施工</h5>
							<div class="service_example_checkarea">
								<p class="text_m service_example_check">カーポート設置</p>
								<p class="text_m service_example_check">カーポート設置</p>
								<p class="text_m service_example_check mb0">カーポート設置</p>
							</div>
							<div class="text-center">
								<p class="h4 pt_marker inline_block mb20"><span class="h0 lh_s semibold numTitle italic">58</span>万円（税抜）</p>
								<p class="text_m text-center">※屋根坪20坪・切妻屋根66㎡</p>
							</div>
						</div>
					</li>
				</ul>
				-->
			</div>
		</div>
	</div>	
</section>
	
<section id="service03" class="pd-common parallax" data-parallax-bg-image="<?php echo get_template_directory_uri(); ?>/img/service_bg03.jpg" data-parallax-bg-position="center" data-parallax-speed="0.4" data-parallax-direction="down">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<div class="under_fv_txtarea pt_bg_white mt50 mb50 mt-xs-20 mb-xs-20 pt_br">
					<p class="numTitle h0 semibold mainColor italic text-center mb0">03</p>
					<h3 class="jpTitle h1 mainColor bold">プライバシーの確保</h3>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="pd-common">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
                <!--
				<div class="text-center">
					<h4 class="jpTitle pt_marker h3 bold inline_block mb40">お客様の想いをカタチに</h4>
				</div>
                -->
				<p class="width800 mb50">「家の裏にアパートが建った」、「庭木を撤去したら、通りから家の中が見えるようになった」など。長くお住いの場所でも、環境の変化によってプライバシーを守ることが難しくなる場合があります。弊社では塀やフェンスの設置、植栽を活用し、お客さまのプライバシーを守るプランをご提案しています。</p>
				<div class="service_warning_area pt_br bgSubColor mb50 width800">
					<h4 class="jpTitle h3 mainColor bold mb30 mb-xs-20 text-center">こんな工事をしています</h4>
					<div class="service_check_list">
						<p class="pt_check_title h4 bold mb10 mb-xs-10">目隠しのためのフェンスの設置</p>
						<p class="pt_check_title h4 bold mb10 mb-xs-10">境界ブロックの設置</p>
						<p class="pt_check_title h4 bold mb10 mb-xs-10">植栽</p>
						<p class="pt_check_title h4 bold mb10 mb-xs-10">防犯カメラや照明の設置</p>
					</div>
				</div>
				<!--
				<h4 class="jpTitle h3 mainColor bold mb20 mb-xs-10 text-center">プライバシーの確保の料金事例</h4>
				<ul class="top_works_ul ul-3 ul-sm-2 ul-xs-1 mb50" data-aos="fade-up">
					<li>
						<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
							<div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
							<h5 class="h3 jpTitle mainColor bold text-center mb10">新築外構施工</h5>
							<div class="service_example_checkarea">
								<p class="text_m service_example_check">カーポート設置</p>
								<p class="text_m service_example_check">カーポート設置</p>
								<p class="text_m service_example_check mb0">カーポート設置</p>
							</div>
							<div class="text-center">
								<p class="h4 pt_marker inline_block mb20"><span class="h0 lh_s semibold numTitle italic">58</span>万円（税抜）</p>
								<p class="text_m text-center">※屋根坪20坪・切妻屋根66㎡</p>
							</div>
						</div>
					</li>
					<li>
						<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
							<div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
							<h5 class="h3 jpTitle mainColor bold text-center mb10">新築外構施工</h5>
							<div class="service_example_checkarea">
								<p class="text_m service_example_check">カーポート設置</p>
								<p class="text_m service_example_check">カーポート設置</p>
								<p class="text_m service_example_check mb0">カーポート設置</p>
							</div>
							<div class="text-center">
								<p class="h4 pt_marker inline_block mb20"><span class="h0 lh_s semibold numTitle italic">58</span>万円（税抜）</p>
								<p class="text_m text-center">※屋根坪20坪・切妻屋根66㎡</p>
							</div>
						</div>
					</li>
					<li>
						<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
							<div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
							<h5 class="h3 jpTitle mainColor bold text-center mb10">新築外構施工</h5>
							<div class="service_example_checkarea">
								<p class="text_m service_example_check">カーポート設置</p>
								<p class="text_m service_example_check">カーポート設置</p>
								<p class="text_m service_example_check mb0">カーポート設置</p>
							</div>
							<div class="text-center">
								<p class="h4 pt_marker inline_block mb20"><span class="h0 lh_s semibold numTitle italic">58</span>万円（税抜）</p>
								<p class="text_m text-center">※屋根坪20坪・切妻屋根66㎡</p>
							</div>
						</div>
					</li>
				</ul>
				-->
			</div>
		</div>
	</div>	
</section>
	
<section id="service04" class="pd-common parallax" data-parallax-bg-image="<?php echo get_template_directory_uri(); ?>/img/service_bg04.jpg" data-parallax-bg-position="center" data-parallax-speed="0.4" data-parallax-direction="down">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<div class="under_fv_txtarea pt_bg_white mt50 mb50 mt-xs-20 mb-xs-20 pt_br">
					<p class="numTitle h0 semibold mainColor italic text-center mb0">04</p>
					<h3 class="jpTitle h1 mainColor bold">その他、外構に関する施工</h3>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="pd-common">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
                <!--
				<div class="text-center">
					<h4 class="jpTitle pt_marker h3 bold inline_block mb40">お客様の想いをカタチに</h4>
				</div>
                -->
				<p class="width800 mb50">ご家族のライフスタイルは、その時どきによって変化します。お子さまの成長やご家族の暮らし方にあわせて外構も使い勝手良く変化させることで、ご自宅での生活を、いつまでも快適に保つことができます。複雑な電気工事など、エクステリア以外の工事が必要な場合は、信頼できる業者をご紹介いたします。屋根や外壁など、どんなお悩みでもご相談ください。</p>
				<div class="service_warning_area pt_br bgSubColor mb50 width800">
					<h4 class="jpTitle h3 mainColor bold mb30 mb-xs-20 text-center">こんな工事をしています</h4>
					<div class="service_check_list">
						<p class="pt_check_title h4 bold mb10 mb-xs-10">ウッドデッキの施工</p>
						<p class="pt_check_title h4 bold mb10 mb-xs-10">サンルーフの取り付け</p>
						<p class="pt_check_title h4 bold mb10 mb-xs-10">防犯カメラの設置</p>
						<p class="pt_check_title h4 bold mb10 mb-xs-10">駐車場のライト設置</p>
						<p class="pt_check_title h4 bold mb10 mb-xs-10">門灯の設置</p>
						<p class="pt_check_title h4 bold mb10 mb-xs-10">インターホンの取り付け、交換</p>
						<p class="pt_check_title h4 bold mb10 mb-xs-10">カーポートのリフォーム</p>
						<p class="pt_check_title h4 bold mb10 mb-xs-10">ブロック塀の修繕</p>
						<p class="pt_check_title h4 bold mb10 mb-xs-10">外構設備の修理</p>
						<p class="pt_check_title h4 bold mb10 mb-xs-10">お墓まわりの修繕</p>
					</div>
				</div>
				<!--                
				<h4 class="jpTitle h3 mainColor bold mb20 mb-xs-10 text-center">お庭の雑草対策の料金事例</h4>
				<ul class="top_works_ul ul-3 ul-sm-2 ul-xs-1 mb50" data-aos="fade-up">
					<li>
						<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
							<div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
							<h5 class="h3 jpTitle mainColor bold text-center mb10">新築外構施工</h5>
							<div class="service_example_checkarea">
								<p class="text_m service_example_check">カーポート設置</p>
								<p class="text_m service_example_check">カーポート設置</p>
								<p class="text_m service_example_check mb0">カーポート設置</p>
							</div>
							<div class="text-center">
								<p class="h4 pt_marker inline_block mb20"><span class="h0 lh_s semibold numTitle italic">58</span>万円（税抜）</p>
								<p class="text_m text-center">※屋根坪20坪・切妻屋根66㎡</p>
							</div>
						</div>
					</li>
					<li>
						<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
							<div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
							<h5 class="h3 jpTitle mainColor bold text-center mb10">新築外構施工</h5>
							<div class="service_example_checkarea">
								<p class="text_m service_example_check">カーポート設置</p>
								<p class="text_m service_example_check">カーポート設置</p>
								<p class="text_m service_example_check mb0">カーポート設置</p>
							</div>
							<div class="text-center">
								<p class="h4 pt_marker inline_block mb20"><span class="h0 lh_s semibold numTitle italic">58</span>万円（税抜）</p>
								<p class="text_m text-center">※屋根坪20坪・切妻屋根66㎡</p>
							</div>
						</div>
					</li>
					<li>
						<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
							<div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
							<h5 class="h3 jpTitle mainColor bold text-center mb10">新築外構施工</h5>
							<div class="service_example_checkarea">
								<p class="text_m service_example_check">カーポート設置</p>
								<p class="text_m service_example_check">カーポート設置</p>
								<p class="text_m service_example_check mb0">カーポート設置</p>
							</div>
							<div class="text-center">
								<p class="h4 pt_marker inline_block mb20"><span class="h0 lh_s semibold numTitle italic">58</span>万円（税抜）</p>
								<p class="text_m text-center">※屋根坪20坪・切妻屋根66㎡</p>
							</div>
						</div>
					</li>
				</ul>
				-->
			</div>
		</div>
	</div>	
</section>
	
</main>

<?php get_footer(); ?>