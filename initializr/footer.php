<!-- フッターナビ ここから -->
<section>
	<ul class="ul-4 ul-xs-2 f_link">
		<li>
			<a href="<?php echo home_url(); ?>/about-us">
				<div class="f_link_img_outer">
					<div class="f_link_title pt_bg_white pt_br">
						<p class="engTitle h3 mainColor relative">About Us</p>
						<h3 class="jpTitle h3 mainColor bold">高塚の想い</h3>
					</div>
					<div class="f_link_filter"></div>
					<div class="f_link_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/f_nav01.jpg');"></div>
				</div>
			</a>
		</li>
		<li>
			<a href="<?php echo home_url(); ?>/service">
				<div class="f_link_img_outer">
					<div class="f_link_title pt_bg_white pt_br">
						<p class="engTitle h3 mainColor relative">Service</p>
						<h3 class="jpTitle h3 mainColor bold">施工プラン</h3>
					</div>
					<div class="f_link_filter"></div>
					<div class="f_link_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/f_nav02.jpg');"></div>
				</div>
			</a>
		</li>
		<li>
			<a href="<?php echo home_url(); ?>/works">
				<div class="f_link_img_outer">
					<div class="f_link_title pt_bg_white pt_br">
						<p class="engTitle h3 mainColor relative">Works</p>
						<h3 class="jpTitle h3 mainColor bold">施工事例</h3>
					</div>
					<div class="f_link_filter"></div>
					<div class="f_link_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/f_nav03.jpg');"></div>
				</div>
			</a>
		</li>
		<li>
			<a href="<?php echo home_url(); ?>/staff">
				<div class="f_link_img_outer">
					<div class="f_link_title pt_bg_white pt_br">
						<p class="engTitle h3 mainColor relative">Staff</p>
						<h3 class="jpTitle h3 mainColor bold">スタッフ紹介</h3>
					</div>
					<div class="f_link_filter"></div>
					<div class="f_link_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/f_nav04.jpg');"></div>
				</div>
			</a>
		</li>
	</ul>
</section>
<!-- フッターナビ ここまで -->

<!-- フッター お問い合わせ ここから -->
<section class="pd-common parallax" data-parallax-bg-image="<?php echo get_template_directory_uri(); ?>/img/f_contact_bg.jpg" data-parallax-bg-position="center bottom" data-parallax-speed="0.4" data-parallax-direction="down">
	<div class="">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="f_contact_area pt_bg_white mt50 mb50 pt_br" data-aos="fade-up">
						<div class="text-center">
							<p class="pt_eng_title engTitle h1 mainColor relative">Contact</p>
							<h3 class="jpTitle h3 mainColor bold mb50 mb-xs-20">エクステリアのことなら、<br>何でもお気軽にお問い合わせください</h3>
						</div>
						<div class="row">
							<div class="col-sm-6 text-center mb-xs-20">
								<a href="<?php echo home_url(); ?>/contact" class="pt_btn bold bgYellowColor yellowBorderColor">お問い合わせ</a>
							</div>
							<div class="col-sm-6 text-center">
								<a href="tel:0866937821" class="pt_btn bold bgYellowColor yellowBorderColor mb10">0866-93-7821</a>
								<p class="lightgrayColor text_m">平日09:00〜18:00</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- フッター お問い合わせ ここまで -->

<section class="f_info">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center text-center-xs fs09">
				<p class="mb20">岡山・倉敷・総社の外構・エクステリア・お庭の工事なら高塚</p>
				<a href="<?php echo home_url(); ?>"><img class="f_logo mb20" src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt=""></a>
				<p class="mb20">岡山県総社市三須616-23</p>
				<p class="mb50">TEL:0866-93-7821</p>
				<p>copyright© 2019 takatsuka all rights reserved.</p>
			</div>
		</div>
	</div>
</section>

<script>
	$('.top_pro_slider').slick({
	  slidesToShow: 4,
	  centerMode: true,
	  arrows: false,
	  autoplay: true,
	  autoplaySpeed: 0, //待ち時間を０に
	  speed: 8000, // スピードをゆっくり
	  swipe: false, // 操作による切り替えはさせない
	  cssEase: 'linear', // 切り替えイージングを'linear'に
	  // 以下、操作後に止まってしまう仕様の対策
	  pauseOnFocus: false,
	  pauseOnHover: false,
	  pauseOnDotsHover: false,

	  // 以下、レスポンシブ
	  responsive: [
		{
		  breakpoint: 767,
		  settings: {
			slidesToShow: 2,
		  }
		}
	  ]
	});
</script>

<?php wp_footer(); ?>
<!--フォントプラス-->
<script type="text/javascript" src="//webfont.fontplus.jp/accessor/script/fontplus.js?tUOPpkwcnxA%3D&box=DPC0QA6Ps9Q%3D&aa=1&ab=2" charset="utf-8"></script>

<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>



<script>
AOS.init({
	placement	: "bottom-top",
	duration: 1000,
});
</script>
</body>
</html>
 