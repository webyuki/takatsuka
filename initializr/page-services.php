<?php get_header(); ?>
<main>

<section class="pageHeader bgMainColor mb100">
	<div class="bgImg bgCircle paddingW imgNone" style="background-image:url('<?php echo get_template_directory_uri();?>/img/bg_circle.png')">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="white mb30">
						<p class="fontEn h2">Services</p>
						<h3 class="h3">オーガのできること</h3>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</section>

<section class="pageServiceMenu mb50">
	<div class="container">
		<div class="row">
			<div class="col-sm-6" data-aos="fade-right">
				<div class="footerMenuBox bgImg tra" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_service_01.jpg);">
					<div class="bgBlack tra">
						<a href="<?php echo home_url();?>/about-us" class="footerMenuBoxText">
							<div class="text-center white">
								<div class="fontEn h4">For Reform</div>
								<h3 class="h3 titleBd mb10">左官を活かしたリフォーム</h3>
								<div class="maruGo bold h4">一般のお客様へ</div>
							</div>

						</a>
					</div>
				</div>
			
			</div>
			<div class="col-sm-6" data-aos="fade-left">
				<div class="footerMenuBox bgImg tra" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_service_02.jpg);">
					<div class="bgBlack tra">
						<a href="<?php echo home_url();?>/about-us" class="footerMenuBoxText">
							<div class="text-center white">
								<div class="fontEn h4">For Clients</div>
								<h3 class="h3 titleBd mb10">大賀の仕事の特徴</h3>
								<div class="maruGo bold h4">メーカー・工務店様へ</div>
							</div>

						</a>
					</div>
				</div>
			
			</div>
		</div>
	
	</div>
</section>


<section class="pageSeReform">
	<div class="container">
		<p class="fontEn h2 mainColor">For Reform</p>
		<h3 class="h3 titleBd mainColor mb10">左官を活かしたリフォーム</h3>
		<p class="maruGo bold h4 mb30">一般のお客様へ</p>
	</div>
	<div class="flex">
		<div class="w55 bgImg spBack" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_service_01.jpg')" data-aos="fade-right">
		</div>
		<div class="w45 spFront" data-aos="fade-left">
			<div class="bgImg bgCircleWhiteReve splitTnner" style="background-image:url('<?php echo get_template_directory_uri();?>/img/bg_circle_blue.png')">
				<h3 class="h4 mainColor mb10 bold">丁寧なヒアリングで最高の仕上がりを</h3>
				<p class="">岡山県の大賀では、お客様の理想を忠実に再現した店舗づくりにこだわっています。左官の確かな技術と豊富な経験を備えた職人が揃っており、丁寧なヒアリングをベースにイメージを膨らませ、最高の仕上がりを実現することが可能です。その腕は多くのお客様、工務店様などから認められており、こだわりのあるご注文にも難なくお応えしております。</p>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="margin" data-aos="fade-up">
			<dl class="pageHospDl mb50">
				<dt class="tra">
					<ul class="inline_block mb10">
						<li class="pageHospHr"><hr></li>
						<li class="maruGo pageHospTitle h4">保証付きの塗装変え工事</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inline_block">
						<li class="bgImg pageHospImg lh_l" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_about_bg_01.png')">塗装は、同じ色を使っても壁の凹凸具合やツヤの違いによって違って見えるものです。よくお客様から「前回の塗り替えの時は3㎝くらいの見本で色を決めなければいけなかった」という声を聞きますが、それではイメージとまったく違う色になってしまうおそれがあります。</li>
					</ul>
				
				</dd>
			</dl>
			<dl class="pageHospDl mb50">
				<dt class="tra">
					<ul class="inline_block mb10">
						<li class="pageHospHr"><hr></li>
						<li class="maruGo pageHospTitle h4">保証付きの塗装変え工事</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inline_block">
						<li class="bgImg pageHospImg lh_l" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_about_bg_01.png')">塗装は、同じ色を使っても壁の凹凸具合やツヤの違いによって違って見えるものです。よくお客様から「前回の塗り替えの時は3㎝くらいの見本で色を決めなければいけなかった」という声を聞きますが、それではイメージとまったく違う色になってしまうおそれがあります。</li>
					</ul>
				
				</dd>
			</dl>
			<dl class="pageHospDl mb50">
				<dt class="tra">
					<ul class="inline_block mb10">
						<li class="pageHospHr"><hr></li>
						<li class="maruGo pageHospTitle h4">保証付きの塗装変え工事</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inline_block">
						<li class="bgImg pageHospImg lh_l" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_about_bg_01.png')">塗装は、同じ色を使っても壁の凹凸具合やツヤの違いによって違って見えるものです。よくお客様から「前回の塗り替えの時は3㎝くらいの見本で色を決めなければいけなかった」という声を聞きますが、それではイメージとまったく違う色になってしまうおそれがあります。</li>
					</ul>
				
				</dd>
			</dl>
			<dl class="pageHospDl mb50">
				<dt class="tra">
					<ul class="inline_block mb10">
						<li class="pageHospHr"><hr></li>
						<li class="maruGo pageHospTitle h4">保証付きの塗装変え工事</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inline_block">
						<li class="bgImg pageHospImg lh_l" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_about_bg_01.png')">塗装は、同じ色を使っても壁の凹凸具合やツヤの違いによって違って見えるものです。よくお客様から「前回の塗り替えの時は3㎝くらいの見本で色を決めなければいけなかった」という声を聞きますが、それではイメージとまったく違う色になってしまうおそれがあります。</li>
					</ul>
				
				</dd>
			</dl>
		</div>
	</div>
</section>


<section class="pageSeWater bgSubColor padding">
	<div class="container">
		<div class="text-center">
			<h3 class="h3 mainColor titleBd mb50">水回り・リフォーム工事も対応可能</h3>
			<div class="width780 mb50">
				<p>岡山県の大賀では、お客様の理想を忠実に再現した店舗づくりにこだわっています。左官の確かな技術と豊富な経験を備えた職人が揃っており、丁寧なヒアリングをベースにイメージを膨らませ、最高の仕上がりを実現することが可能です。その腕は多くのお客様、工務店様などから認められており、こだわりのあるご注文にも難なくお応えしております。</p>
			</div>
		</div>
		<div class="row" data-aos="fade-up">
			<div class="col-sm-3">
				<div class="pageAboutSakanUl mb30 clearfix">
					<div class="bgImg relative mb10" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_sakan_01.jpg')">
						<div class="absolute fontEnSerif italic bgNum">01</div>
					</div>
					<h4 class="maruGo bold h5 mb10">水回りのリフォーム</h4>
					<p class="text_m gray">施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="pageAboutSakanUl mb30 clearfix">
					<div class="bgImg relative mb10" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_sakan_01.jpg')">
						<div class="absolute fontEnSerif italic bgNum">01</div>
					</div>
					<h4 class="maruGo bold h5 mb10">水回りのリフォーム</h4>
					<p class="text_m gray">施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="pageAboutSakanUl mb30 clearfix">
					<div class="bgImg relative mb10" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_sakan_01.jpg')">
						<div class="absolute fontEnSerif italic bgNum">01</div>
					</div>
					<h4 class="maruGo bold h5 mb10">水回りのリフォーム</h4>
					<p class="text_m gray">施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="pageAboutSakanUl mb30 clearfix">
					<div class="bgImg relative mb10" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_sakan_01.jpg')">
						<div class="absolute fontEnSerif italic bgNum">01</div>
					</div>
					<h4 class="maruGo bold h5 mb10">水回りのリフォーム</h4>
					<p class="text_m gray">施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。</p>
				</div>
			</div>
		</div>
		<div class="row" data-aos="fade-up">
			<div class="col-sm-3">
				<div class="pageAboutSakanUl mb30 clearfix">
					<div class="bgImg relative mb10" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_sakan_01.jpg')">
						<div class="absolute fontEnSerif italic bgNum">01</div>
					</div>
					<h4 class="maruGo bold h5 mb10">水回りのリフォーム</h4>
					<p class="text_m gray">施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="pageAboutSakanUl mb30 clearfix">
					<div class="bgImg relative mb10" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_sakan_01.jpg')">
						<div class="absolute fontEnSerif italic bgNum">01</div>
					</div>
					<h4 class="maruGo bold h5 mb10">水回りのリフォーム</h4>
					<p class="text_m gray">施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="pageAboutSakanUl mb30 clearfix">
					<div class="bgImg relative mb10" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_sakan_01.jpg')">
						<div class="absolute fontEnSerif italic bgNum">01</div>
					</div>
					<h4 class="maruGo bold h5 mb10">水回りのリフォーム</h4>
					<p class="text_m gray">施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="pageAboutSakanUl mb30 clearfix">
					<div class="bgImg relative mb10" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_sakan_01.jpg')">
						<div class="absolute fontEnSerif italic bgNum">01</div>
					</div>
					<h4 class="maruGo bold h5 mb10">水回りのリフォーム</h4>
					<p class="text_m gray">施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。施工内容。</p>
				</div>
			</div>
		</div>
	</div>
	
</section>


<section class="pageSeReform padding mb50 bgMainColor">
	<div class="container">
		<p class="fontEn h2 white">For Clients</p>
		<h3 class="h3 titleBd white mb10">大賀の仕事の特徴</h3>
		<p class="maruGo bold h4 mb30 white">メーカー・工務店様へ</p>
	</div>
	<div class="flex">
		<div class="w45 spFront" data-aos="fade-right">
			<div class="bgImg bgCircleWhiteReve splitTnner" style="background-image:url('<?php echo get_template_directory_uri();?>/img/bg_circle_blue.png')">
				<h3 class="h4 white mb10 bold">丁寧なヒアリングで最高の仕上がりを</h3>
				<p class="white">岡山県の大賀では、お客様の理想を忠実に再現した店舗づくりにこだわっています。左官の確かな技術と豊富な経験を備えた職人が揃っており、丁寧なヒアリングをベースにイメージを膨らませ、最高の仕上がりを実現することが可能です。その腕は多くのお客様、工務店様などから認められており、こだわりのあるご注文にも難なくお応えしております。</p>
			</div>
		</div>
		<div class="w55 bgImg spBack" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_service_01.jpg')" data-aos="fade-left">
		</div>
	</div>
</section>

<section class="">
	<div class="bgImg bgCircle" style="background-image:url('<?php echo get_template_directory_uri();?>/img/bg_circle_white_left.png')">
		<div class="container">
			<div class="text-center">
				<h3 class="h3 mainColor titleBd mb50">アフターサービスについて</h3>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<h3 class="h4 mainColor mb10 bold">丁寧なヒアリングで最高の仕上がりを</h3>
					<p class="">岡山県の大賀では、お客様の理想を忠実に再現した店舗づくりにこだわっています。左官の確かな技術と豊富な経験を備えた職人が揃っており、丁寧なヒアリングをベースにイメージを膨らませ、最高の仕上がりを実現することが可能です。</p>
				</div>
				<div class="col-sm-6" data-aos="fade-up">
					<ul class="inline_block pageAboutSakanUl">
						<li>
							<div class="bgImg relative mb10" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_sakan_01.jpg')">
								<div class="absolute fontEnSerif italic bgNum">01</div>
							</div>
							<p class="text_m">左官業でやること、大切にしていること。左官業でやること、大切にしていること。</p>
						</li>
						<li>
							<div class="bgImg relative mb10" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_sakan_01.jpg')">
								<div class="absolute fontEnSerif italic bgNum">01</div>
							</div>
							<p class="text_m">左官業でやること、大切にしていること。左官業でやること、大切にしていること。</p>
						</li>
						<li>
							<div class="bgImg relative mb10" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_sakan_01.jpg')">
								<div class="absolute fontEnSerif italic bgNum">01</div>
								
							</div>
							<p class="text_m">左官業でやること、大切にしていること。左官業でやること、大切にしていること。</p>
						</li>
					</ul>



				</div>
			</div>
		</div>
	</div>
</section>


<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>