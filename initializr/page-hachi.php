<?php get_header(); ?>
<main>

<section class="pageHeader bgImg margin bgMainColor" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv_hachi.png')">
	<div class="container">
		<div class="white">
			<h2 class="bold h3">ハチ駆除</h2>
			<h3 class="titleHeader mincho subColor">蜂</h3>
			<div class="row">
				<div class="col-sm-6">
					<p class="text_m white">ハチは建物の新旧にかかわらず、非常に小さな隙間を見つけて侵入します。床下・天井裏などの狭い場所や駆除効果の高い夜間の作業、巣の後処理なども安心してお任せください。</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="margin pageGreeting">
	<div class="container">
		<h3 class="bold h3 text-center mb10">建物の新旧にかかわらず、ハチは小さな隙間からでも侵入します</h3>
		<div class="titleBd mb10"></div>
		<p class="fontEn h5 bold mainColor text-center mb30">Danger</p>
		<div class="row mb30">
			<div class="col-sm-6">
				<h4 class="h3 bold mainColor mb10">天井裏や床下の巣は、気づいた時には巨大に</h4>
				<p>どんな新しいお住まいにも、小さな隙間は必ずあります。古くなった建物だけでなく、建てたばかりのお家でもハチは侵入してきます。軒下など外から見える場所は発見しやすいのですが、天井裏や床下の巣は気がついた時には手に負えないほど大きくなっていることも。ハチは強力な針を持ち、刺された場合は痛みを感じるだけでなくアレルギーを引き起こすこともあります。発見した場合はすみやかにご相談ください。</p>
			</div>
			<div class="col-sm-6">
				<img class="" src="<?php echo get_template_directory_uri();?>/img/page_hachi_01.jpg" alt="">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-sm-push-6">
				<h4 class="h3 bold mainColor mb10">夜間の一斉駆除と巣の後処理</h4>
				<p>ハチのなかでも特に大きな被害を及ぼすスズメバチは、昼間エサを取りに巣の外へ出ており、巣を駆除しても夜になると巣のあった場所に戻ってきます。ヒカリ消毒ではハチが巣に戻り、なおかつ視力が低下する夜を狙ってハチごと巣を駆除します。またミツバチの蜜など巣とともに残されたものも、最後まで責任をもって処理いたします。</p>
			</div>
			<div class="col-sm-6 col-sm-pull-6">
				<img class="" src="<?php echo get_template_directory_uri();?>/img/top_future_bg.jpg" alt="">
			</div>
		</div>
	</div>
</section>

	
<section class="margin bgGreen pageCommonRecommend">
	<div class="container">
		<h3 class="bold h3 text-center mb10">このような方に<br>「ハチ駆除」をオススメします</h3>
		<div class="titleBd mb10"></div>
		<p class="fontEn h5 bold mainColor text-center mb30">Recommend</p>
		
		<div class="pageCommonRecommendBox">
			<ul class="inline_block h4 bold">
				<li><i class="fa fa-check-circle"></i>巣は見当たらないが、家のまわりをハチがよく飛ぶようになった</li>
				<li><i class="fa fa-check-circle"></i>床下や天井裏から羽音がする</li>
				<li><i class="fa fa-check-circle"></i>巣は見えているが、近寄るのがこわい</li>
			</ul>
		</div>		
		
	</div>
</section>

<section class="topWorks margin">
	<div class="container">
		<h3 class="bold h3 mb10">実績紹介</h3>
		<div class="titleBd mb10 titleBdLeft"></div>
		<p class="fontEn h5 bold mainColor mb30">Works</p>
		<div class="row mb30">

        	<?php
				$args = array(
					'post_type' => 'works', //投稿タイプ名
					'posts_per_page' => 3, //出力する記事の数
					'tax_query' => array(
						array(
							'taxonomy' => 'works_cate', //タクソノミー名
							'field' => 'slug',
							//'terms' => $term_slug //タームのスラッグ
							'terms' => 'hachi' //タームのスラッグ
						)
					)
				);
			?>
			<?php
				$myposts = get_posts( $args );
				foreach ( $myposts as $post ) : setup_postdata( $post );
			?>

			<?php get_template_part('content-post-works'); ?>

            <?php endforeach; ?>

		</div>
		<a href="<?php echo home_url();?>/works_cate/hachi/" class="button white tra text-center">詳しく見る</a>
	</div>
	
	
</section>


<section class="pageCommonFee margin bgGreen">
	<div class="container">
		<h3 class="bold h3 text-center mb10">ハチ駆除の料金</h3>
		<div class="titleBd mb10"></div>
		<p class="fontEn h5 bold mainColor text-center mb30">Prices</p>
		<div class="row">
			<div class="col-sm-4">
				<div class="pageCommonFeeBox mb30">
					<h5 class="mainColor h4 bold">足長バチ駆除</h5>
					<p class="pageFeeColor"><span class="h2 bold">5,000</span>円～</p>
					<p class="grayColor text_m pageCommonFeeBoxBorder">※大きさ、種類によります。</p>
					<ul class="text_m">
						<li><i class="fa fa-check mainColor"></i>調査・診断・見積もり無料</li>
					</ul>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="pageCommonFeeBox mb30">
					<h5 class="mainColor h4 bold">スズメバチ駆除</h5>
					<p class="pageFeeColor"><span class="h2 bold">10,000</span>円～</p>
					<p class="grayColor text_m pageCommonFeeBoxBorder">※大きさ、種類によります。</p>
					<ul class="text_m">
						<li><i class="fa fa-check mainColor"></i>調査・診断・見積もり無料</li>
					</ul>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="pageCommonFeeBox mb30">
					<h5 class="mainColor h4 bold">ミツバチ駆除</h5>
					<p class="pageFeeColor"><span class="h2 bold">30,000</span>円～</p>
					<p class="grayColor text_m pageCommonFeeBoxBorder">※大きさ、種類によります。</p>
					<ul class="text_m">
						<li><i class="fa fa-check mainColor"></i>調査・診断・見積もり無料</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_template_part('parts/temp-flow'); ?>

<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>