<?php get_header(); ?>
<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
	<div class="container">
		<h1>Hello, world!</h1>
		<p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
		<p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>
	</div>
</div>



<section class="area_single">
	<?php 
		while ( have_posts() ) : the_post();
	?>
	<div class="container">
		<?php breadcrumb(); ?>
		<div class="row">
			<div class="col-sm-9">
				<div class="entry">
					<div class="title_bg title_margin">
						<h2 class="h2 title_main  bold"><?php the_title();?></h2>
					</div>				
					<!--
					<h3 class="h3 title_sub bold title_margin">サブタイトルサブタイトルサブタイトル</h3>
					-->
					<?php remove_filter ('the_content', 'wpautop'); ?>
					<?php the_content();?>
				あああああ
				</div>





				
				
				<!--
				<div class="row">
					<div class="col-md-4">
					<h2>Heading</h2>
					<p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
					<p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
					</div>
					<div class="col-md-4">
					<h2>Heading</h2>
					<p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
					<p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
					</div>
					<div class="col-md-4">
					<h2>Heading</h2>
					<p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
					<p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
					</div>
				</div>
				-->
			</div>
			<div class="col-sm-3">
				<?php get_sidebar(); ?>
   			</div>
		</div>
	</div>
	<?php 
		endwhile;
	?>	
</section>
<?php get_footer(); ?>