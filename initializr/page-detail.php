<?php get_header(); ?>

<main class="pt_bg_dot">
	
<section class="pd-common parallax under_fv" data-parallax-bg-image="<?php echo get_template_directory_uri(); ?>/img/works_fv.jpg" data-parallax-bg-position="center" data-parallax-speed="0.4" data-parallax-direction="down">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<div class="under_fv_txtarea pt_bg_white mt140 mt-xs-80 mb50 pt_br">
					<p class="engTitle h1 mainColor relative">Works</p>
					<h2 class="jpTitle h1 bold">施工事例</h2>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="pd-common">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="h1 jpTitle mainColor bold text-center mb30">岡山市 M様邸 カーポート設置</h3>
				<div class="text-center mb30">
					<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">岡山市</p>
					<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">カーポート設置</p>
				</div>
				<div class="detail_main_imgarea">
					<img src="<?php echo get_template_directory_uri(); ?>/img/works_detail_sample.jpg" alt="">
				</div>
			</div>
		</div>
	</div>
	<div class="container detail_txtarea mb50">
		<div class="row">
			<div class="col-sm-6">
				<h4 class="h3 mainColor bold mb20">フレームの重なりで立体感を出した門まわり変更を提案</h4>
				<p class="mb20">K様のお宅を現調させていただいた時に一番心配になったのが屋根の状態です。<br>カラーベストがミルフィーユ状態に劣化していました。<br>K様もこのままではいけないと認知されており、板金の屋根にやりかえる方法での見積もりをさせて頂きました。</p>
				<p class="mb20">他社との比較もして頂き、他社と弊社の工事内容の違いや弊社としての提案では作業方法や使用材料を変えて安くすることはできないラインのご説明をさせて頂きました。<br>悩まれた結果、見積の内容が細かくて施工もしっかりとしてくれるだろうと工事を依頼して頂きました。<br>想いが伝わって良かったです！</p>
			</div>
			<div class="col-sm-6">
				<div class="detail_table_outer bgLightBlueColor pt_br">
					<p class="h1 white engTitle text-center mb30 mb-xs-10">Data</p>
					<table class="detail_table white">
						<tbody>
							<tr>
								<th>施工エリア</th>
								<td>岡山市</td>
							</tr>
							<tr>
								<th>施工カテゴリ</th>
								<td>カーポート設置</td>
							</tr>
							<tr>
								<th>投稿日</th>
								<td>2019.10.01</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="container detail_txtarea">
		<div class="row">
			<div class="col-sm-12">
				<p class="detail_before_title h3 numTitle mainColor italic mb10">Before</p>
				<ul class="detail_before_ul ul-4 ul-xs-2">
					<li>
						<a href="<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg" data-lightbox="before" data-title="下葺材撤去">
							<div class="detail_before_ul_img_outer">
								<div class="detail_before_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
							</div>
							<p class="detail_before_ul_title bgYellowColor text_m">下葺材撤去</p>
						</a>
					</li>
					<li>
						<a href="<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg" data-lightbox="before" data-title="下葺材撤去">
							<div class="detail_before_ul_img_outer">
								<div class="detail_before_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
							</div>
							<p class="detail_before_ul_title bgYellowColor text_m">下葺材撤去</p>
						</a>
					</li>
					<li>
						<a href="<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg" data-lightbox="before" data-title="下葺材撤去">
							<div class="detail_before_ul_img_outer">
								<div class="detail_before_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
							</div>
							<p class="detail_before_ul_title bgYellowColor text_m">下葺材撤去</p>
						</a>
					</li>
					<li>
						<a href="<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg" data-lightbox="before" data-title="下葺材撤去">
							<div class="detail_before_ul_img_outer">
								<div class="detail_before_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
							</div>
							<p class="detail_before_ul_title bgYellowColor text_m">下葺材撤去</p>
						</a>
					</li>
				</ul>
			</div>
			<div class="col-sm-12">
				<p class="detail_before_title h3 numTitle mainColor italic mb10">After</p>
				<ul class="detail_before_ul ul-4 ul-xs-2">
					<li>
						<a href="<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg" data-lightbox="after" data-title="下葺材撤去">
							<div class="detail_before_ul_img_outer">
								<div class="detail_before_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
							</div>
							<p class="detail_before_ul_title bgYellowColor text_m">下葺材撤去</p>
						</a>
					</li>
					<li>
						<a href="<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg" data-lightbox="after" data-title="下葺材撤去">
							<div class="detail_before_ul_img_outer">
								<div class="detail_before_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
							</div>
							<p class="detail_before_ul_title bgYellowColor text_m">下葺材撤去</p>
						</a>
					</li>
					<li>
						<a href="<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg" data-lightbox="after" data-title="下葺材撤去">
							<div class="detail_before_ul_img_outer">
								<div class="detail_before_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
							</div>
							<p class="detail_before_ul_title bgYellowColor text_m">下葺材撤去</p>
						</a>
					</li>
					<li>
						<a href="<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg" data-lightbox="after" data-title="下葺材撤去">
							<div class="detail_before_ul_img_outer">
								<div class="detail_before_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
							</div>
							<p class="detail_before_ul_title bgYellowColor text_m">下葺材撤去</p>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
	
<section class="pd-common bgSubColor pt_bg_border">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center">
					<p class="pt_eng_title engTitle h1 mainColor relative">Others</p>
					<h3 class="jpTitle h1 mainColor bold mb50 mb-xs-20">他の事例を見る</h3>
				</div>
				<ul class="top_works_ul ul-3 ul-sm-2 ul-xs-1 mb50 mb-xs-30">
					<li>
						<a href="">
							<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
								<div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
								<p class="h4 jpTitle mainColor bold mb10">岡山市 M様邸 カーポート設置</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">岡山市</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">カーポート設置</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">岡山市</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">カーポート設置</p>
							</div>
						</a>
					</li>
					<li>
						<a href="">
							<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
								<div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
								<p class="h4 jpTitle mainColor bold mb10">岡山市 M様邸 カーポート設置</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">岡山市</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">カーポート設置</p>
							</div>
						</a>
					</li>
					<li>
						<a href="">
							<div class="top_works_area mainBorderColor bgWhiteColor pt_br matchHeight">
								<div class="top_works_img bg-common pt_br mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_work_sample.jpg');"></div>
								<p class="h4 jpTitle mainColor bold mb10">岡山市 M様邸 カーポート設置</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">岡山市</p>
								<p class="top_works_tag text_s bgWhiteColor yellowBorderColor mr10">カーポート設置</p>
							</div>
						</a>
					</li>
				</ul>
				<div class="text-center">
					<a href="<?php echo home_url(); ?>/works" class="pt_btn bold bgMainColor mainBorderColor">施工事例</a>
				</div>
			</div>
		</div>
	</div>	
</section>

</main>






<?php get_footer(); ?>