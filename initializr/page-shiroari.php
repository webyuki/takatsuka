<?php get_header(); ?>
<main>

<section class="pageHeader bgImg margin bgMainColor" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv_shiroari.png')">
	<div class="container">
		<div class="white">
			<h2 class="bold h3">シロアリ駆除・予防</h2>
			<h3 class="titleHeader mincho subColor">白蟻</h3>
			<div class="row">
				<div class="col-sm-6">
					<p class="text_m white">シロアリは木造家屋以外の建物にも被害を与えます。地中から床下に侵入するため被害の大きさがわかりづらく、完全な駆除が難しい害虫です。少しでも気になる場合はプロにご相談ください。</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="margin pageGreeting">
	<div class="container">
		<h3 class="bold h3 text-center mb10">シロアリはどんな家にも被害を与えます</h3>
		<div class="titleBd mb10"></div>
		<p class="fontEn h5 bold mainColor text-center mb30">Danger</p>
		<div class="row mb30">
			<div class="col-sm-6">
				<h4 class="h3 bold mainColor mb10">「木造家屋じゃないから大丈夫」は間違いです</h4>
				<p>シロアリは地面の中に巣を作り、地中を通って建物に侵入します。地面に直接触れている柱はもちろん、コンクリートの土台の上に立てられた木材にも大きな被害を及ぼします。「わが家は木造家屋じゃないから大丈夫」は間違い。日本で建築された家屋にはドア周りや建具など、必ずどこかに木材が使用されています。たとえ地面から離れた場所であっても、被害にあう可能性はあります。</p>
			</div>
			<div class="col-sm-6">
				<img class="" src="<?php echo get_template_directory_uri();?>/img/page_shiro_01.jpg" alt="">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-sm-push-6">
				<h4 class="h3 bold mainColor mb10">羽アリが気になった時にはすでに、多くの建物は3年以上の被害を受けています</h4>
				<p>シロアリが発生しているサインとして、「羽アリが飛んでいたら要注意」ということが言われます。ところが羽アリが発生した時はすでに、建物は数年にわたってシロアリの食害を受けており、床下には大きな被害が広がっていることがあります。ヒカリ消毒では、離れた場所にある巣から徹底的に駆除を行い、建物の構造に合った方法で再発を防ぎます。</p>
			</div>
			<div class="col-sm-6 col-sm-pull-6">
				<img class="" src="<?php echo get_template_directory_uri();?>/img/page_shiro_02.jpg" alt="">
			</div>
		</div>
	</div>
</section>

	
<section class="margin bgGreen pageCommonRecommend">
	<div class="container">
		<h3 class="bold h3 text-center mb10">このような方に<br>「シロアリ駆除・予防」をオススメします</h3>
		<div class="titleBd mb10"></div>
		<p class="fontEn h5 bold mainColor text-center mb30">Recommend</p>
		
		<div class="pageCommonRecommendBox">
			<ul class="inline_block h4 bold">
				<li><i class="fa fa-check-circle"></i>新築から5〜10年経ち、ハウスメーカーや専門業者による保証が終了している方</li>
				<li><i class="fa fa-check-circle"></i>5年以上シロアリ駆除や予防をしていない方</li>
				<li><i class="fa fa-check-circle"></i>床がフワフワしていると感じた方</li>
				<li><i class="fa fa-check-circle"></i>羽アリを見つけた方</li>
			</ul>
		</div>		
		
	</div>
</section>

<section class="topWorks margin">
	<div class="container">
		<h3 class="bold h3 mb10">実績紹介</h3>
		<div class="titleBd mb10 titleBdLeft"></div>
		<p class="fontEn h5 bold mainColor mb30">Works</p>
		<div class="row mb30">
		
        	<?php
				$args = array(
					'post_type' => 'works', //投稿タイプ名
					'posts_per_page' => 3, //出力する記事の数
					'tax_query' => array(
						array(
							'taxonomy' => 'works_cate', //タクソノミー名
							'field' => 'slug',
							//'terms' => $term_slug //タームのスラッグ
							'terms' => 'shiroari' //タームのスラッグ
						)
					)
				);
			?>
			<?php
				$myposts = get_posts( $args );
				foreach ( $myposts as $post ) : setup_postdata( $post );
			?>

			<?php get_template_part('content-post-works'); ?>

            <?php endforeach; ?>
        </div>
		<a href="<?php echo home_url();?>/works_cate/shiroari/" class="button white tra text-center">詳しく見る</a>
	</div>
	
	
</section>


<section class="pageCommonFee margin bgGreen">
	<div class="container">
		<h3 class="bold h3 text-center mb10">シロアリ駆除の料金</h3>
		<div class="titleBd mb10"></div>
		<p class="fontEn h5 bold mainColor text-center mb30">Prices</p>
		<div class="row">
			<div class="col-sm-4"></div>
			<div class="col-sm-4">
				<div class="pageCommonFeeBox mb50">
					<h5 class="mainColor h4 bold">シロアリ駆除</h5>
					<p class="pageFeeColor">㎡／<span class="h2 bold">1,200</span>円～</p>
					<p class="grayColor text_m pageCommonFeeBoxBorder">※構造・シロアリの有無によりますので、お問い合わせください。</p>
					<ul class="text_m">
						<li><i class="fa fa-check mainColor"></i>調査・診断・見積もり無料</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-1"></div>
			<div class="col-sm-10">
				<div class="pageCommonFeeNotice text-center">
					<img class="pageCommonFeeNoticeIco" src="<?php echo get_template_directory_uri();?>/img/page_notice_icon.png" alt="">
					<p class="pageFeeColor h3 bold">＼安心の<span class="h1 bold">5年</span>保証／</p>
					<h5 class="mainColor h4 bold mb10">施工から5年間無償で再施工お約束します</h5>
					<p class="text_m">弊社では自信を持って駆除をしているため、独自の保証・アフターケアを設けております。<br>保証期間外でも何か少しでも気になることがありましたらお気軽にお問い合わせください。</p>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_template_part('parts/temp-flow'); ?>

<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>